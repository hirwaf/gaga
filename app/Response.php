<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    //
    public function information(){
        return $this->belongsTo('App\Information');
    }
    
    public function question(){
        return $this->belongsTo('App\Question');
    }
    
    public function answer(){
        return $this->belongsTO('App\Answer');
    }
}
