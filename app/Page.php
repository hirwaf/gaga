<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    //

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = ['name','slug','static','elix','content'];

    public function subpage(){
        return $this->hasMany('App\SubPage');
    }
    
    public function menu(){
        return $this->hasOne('App\Menu');
    }

    public function pagecontent(){
    	return $this->hasOne('App\PageContent');
    }
}
