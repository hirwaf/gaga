<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    //

	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = ['firstname','lastname','sex','telephone','email','images','address','information','approved'];

    public function user(){
        $this->hasOne('App\User','information_id');
    }
    
    public function response(){
        $this->hasMany('App\Response');
    }
    
}
