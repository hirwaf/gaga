<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'web'], function(){
 
    Route::get('/', [
        'as'    =>  'index.web',
        'uses'  =>  'WebController@index'
    ]);
    
    Route::get("/email", function(){
        return redirect()->away('https://privateemail.com/appsuite');
    });

    Route::get('/welcome','WebController@index');
    Route::post('/welcome','WebController@subscribe');
    Route::resource('/registration','RegistrationController');
    Route::resource('/courses','CourseController');
    Route::post('/gallery',[
        'as'    =>  'gallery.destroy',
        'uses'  =>  'GalleryController@destroy'
    ]);
    Route::resource('/gallery','GalleryController@index',[
        'only'  =>  [
            'index'
        ]
    ]);

    Route::get('/course/{course}',[
        'as'    =>  'course.show',
        'uses'  =>  'CourseController@show'
    ])->middleware(['auth','payed']);
    
    Route::post('/getsector','RegistrationController@getSector');
    Route::get('/getsector',function(){
        return abort(404);
    });
    
    Route::get('/nopayment', [
    	'as'	=>	'nopayment',
    	'uses'	=>	'nopayment@no'
    ]);

    Route::get('/contact-us',[
        'as'    =>  'contact.us.web',
        'uses'  =>  'ContactUsController@getContactUs'
    ]);

    Route::post('/contact-us',[
        'as'    =>  'contact.us.post.web',
        'uses'  =>  'ContactUsController@postContactUs'
    ]);

    Route::get('read/{id}',"CourseController@read");
    Route::get('quiz',[
        'as'    =>  'doquiz',
        'uses'  =>  "QuizController@doQuiz"
    ]);
    Route::post('student/answers', [
        'as'    =>  'submit.answers',
        'uses'  =>  'QuizController@postUserAnswers'
    ]);
    Route::post('quiz',[
        'as'    =>  'next.to.question',
        'uses'  =>  'QuizController@doQuizPostCredit'
    ]);

    Route::get('statistics', [
        'as'    =>  'statistics.show',
        'uses'  =>  'QuizController@statistics'
    ]);

    Route::get('certificates', [
        'as'    =>  'certificates',
        'uses'  =>  'QuizController@certificates'
    ]);

    Route::get('view/{id}/{token}', [
        'as'    =>  'viewcert',
        'uses'   =>  "QuizController@certificate"
    ]);

    Route::get('/{slug}/', [
        'as'    =>  'with.slug.web',
        'uses'  =>  'WebController@staticPage'
    ]);
    
    //Auth Routes
    Route::auth();

    //User Prefix
    Route::group(['prefix' => 'client'],function(){
        
        Route::get('/login',[
            'as'    =>  'login.user',
            'uses'  =>  'Auth\AuthController@getLogin'
        ]);
        
        Route::post('/login',[
            'as'    =>  'post.login.user',
            'uses'  =>  'Auth\AuthController@login'
        ]);
        
        Route::group([ 'namespace' => 'Users', 'middleware' => 'auth' ], function(){
            Route::get('/dashboard',[
                'as'    =>  'dashboard.user',
                'uses'  =>  'UserController@dashboard'
            ]);
            Route::get('/profile', [
                'as'    =>  'profile.user',
                'uses'  =>  'UserController@profile'
            ]);
        });

        Route::get('/logout',[
            'as'    =>  'logout.user',
            'uses'  =>  'Auth\AuthController@logout'
        ]);


    });
    
    
    
    //Admin Route
    

    //Admin Middleware Group
    Route::group([ 'prefix' => 'admin'],function(){
        
        Route::get('/login', [
            'as'    =>  'login.super',
            'uses'  =>  'Auth\SuperController@getLogin'
        ]);
        Route::post('/login',[
            'as'    =>  'login.super',
            'uses'  =>  'Auth\SuperController@login'
        ]);
        //Logout
        Route::get('/logout',[
            'as'    =>  'logout.super',
            'uses'  =>  'Auth\SuperController@logout'
        ]);
        
        Route::group([ 'namespace' => 'Super', 'middleware' => 'admin' ],function(){
            
            
            Route::get('/dashboard', [
                'as'   => 'dashboard.super', 
                'uses' => 'SuperController@getDashboard'
            ]);
            //Users
            Route::get('/profile', [
                'as'    =>  'profile.super',
                'uses'  =>  'SuperController@getProfile'
            ]);
            Route::post('/profile', [
                'uses'    =>    'SuperController@updateProfile',
                'as'      =>    'post.profile.super'
            ]);

            Route::post('/send', [
                'as'    =>  'send.email.super',
                'uses'  =>  'SuperController@sendEmail'
            ]);

            Route::post('/markasread',"SuperController@markAsRead");
            
            Route::resource('subscribes',"SubscribesController",[
                'names' =>  [
                    'index' =>  'subcribes.index'
                ]
            ]);

            /*
                Clients Section    
             */
            Route::group(['prefix' => 'clients'], function(){
                Route::get('/all', 'UsersController@index');
                Route::get('/', [
                    'as'    =>  'all.user.super',
                    'uses'  =>  'UsersController@index'
                ]);
                Route::get('/allowed', [
                    'as'    =>  'allowed.user.super',
                    'uses'  =>  'UsersController@getAllowed'
                ]);
                Route::get('/not-allowed',[
                    'as'    =>  'not.allowed.user.super',
                    'uses'  =>  'UsersController@getNotAllowed'
                ]);
                Route::post('/allow', [
                    'as'    =>  'allow.user.super',
                    'uses'  =>  'UsersController@allow'
                ]);
                Route::get('/add', [
                    'as'    =>  'add.user.super',
                    'uses'  =>  'UsersController@create'
                ]);
                Route::post('/add', [
                    'as'    =>  'save.user.super',
                    'uses'  =>  'UsersController@store'
                ]);
                Route::post('/reverse', [
                    'as'    =>  'reverse.user.super',
                    'uses'  =>  'UsersController@reverseApproval'
                ]);
                Route::post('/',[
                    'as'    =>  'delete.user.super',
                    'uses'  =>  'UsersController@destroy'
                ]);
                Route::get("/{id}", [
                    'as'    =>  'view.user.super',
                    'uses'  =>  'UsersController@show'
                ]);
            });
            /*
                Pages Section    
             */
            Route::group(['prefix' => 'pages' ], function(){
                Route::post('/edit', [
                    'as'    =>  'update.page.super',
                    'uses'  =>  'PagesController@update'
                ]);
                Route::get('/menu', [
                    'as'    =>  'menu.page.super',
                    'uses'  =>  'PagesController@getMenu'
                ]);
                Route::post('/ordermenu', [
                    'as'    =>  'order.menu.super',
                    'uses'  =>  'PagesController@orderMenu'
                ]);
                Route::post('/menu', [
                    'as'    =>  'status.menu.super',
                    'uses'  =>  'PagesController@statusMenu'
                ]);
                Route::get('/static/{id}',[
                    'as'    =>  'static.page.super',
                    'uses'  =>  'StaticController@edit'
                ]);
                Route::post('/static', [
                    'as'    =>  'static.page.super',
                    'uses'  =>  'StaticController@update'
                ]);
                Route::get('/static', function(){
                    abort(404);
                });
                Route::post('/destroy',[
                    'as'    =>  'destroy.page.super',
                    'uses'  =>  'PagesController@destroy'
                ]);

                // Route::get('/edit/{id}', function($id){
                //     return $id;
                // });

            });
            Route::resource('pages', 'PagesController', [
                'names' =>  [
                    'index'     =>  'all.pages.super',
                    'create'    =>  'create.page.super',
                    'store'     =>  'store.page.super',
                    'edit'      =>  'edit.page.super'
                ],
                'only'  =>  [
                    'index','show', 'edit','create','store'
                ]
            ] );
            /*
                Sub-Pages Section    
             */
            Route::group(['prefix' =>   'subpages' ], function(){
                Route::post('/edit',[
                    'as'    =>  'update.subpage.super',
                    'uses'  =>  'SubPagesController@update'
                ]);
                Route::post('/destroy',[
                    'as'    =>  'destroy.subpage.super',
                    'uses'  =>  'SubPagesController@destroy'
                ]);
            });
            Route::resource('subpages', 'SubPagesController', [
                'names' =>  [
                    'index'     =>  'all.subpages.super',
                    'create'    =>  'create.subpage.super',
                    'store'     =>  'store.subpage.super',
                    'edit'      =>  'edit.subpage.super'
                ],
                'only'  =>  [
                    'index','show','create','edit','store'
                ]
            ] );
            /*
                Courses Section    
             */
            Route::group(['prefix' =>   'courses' ], function(){

                Route::post('/edit', [
                    'as'    =>  'update.course.super',
                    'uses'  =>  'CoursesController@update'
                ]);
                Route::post('/destroy',[
                    'as'    =>  'delete.course.super',
                    'uses'  =>  'CoursesController@destroy'
                ]);
            });
            Route::resource('courses', 'CoursesController', [ 'names' => [
                'index'     =>  'all.courses.super',
                'create'    =>  'create.course.super',
                'store'     =>  'store.course.super',
                'edit'      =>  'edit.course.super',

            ],  'only'      =>  [
                'index','show','create','edit','store'
            ]   ]);
            /*
                Credit Section    
             */
            Route::group(['prefix' =>   'credits' ], function(){

                Route::post('/edit', [
                    'as'    =>  'update.credit.super',
                    'uses'  =>  'CreditsController@update'
                ]);
                Route::post('/add',[
                    'as'    =>  'store.credit.super',
                    'uses'  =>  'CreditsController@store'
                ]);
                Route::post('/destroy', [
                    'as'    =>  'destroy.credit.super',
                    'uses'  =>  'CreditsController@destroy'
                ]); 
    //            
                Route::get('/read/{title}', [
                    'as'    =>  'read.credit.super',
                    'uses'  =>  'CreditsController@read'
                ]);
            });
            Route::resource('credits', 'CreditsController', [ 'names' => [
                'index'     =>  'all.credits.super',
                'create'    =>  'create.credit.super',
                'store'     =>  'store.credit.super',
                'edit'      =>  'edit.credit.super',

            ],  'only'      =>  [
                'index','show','create','edit','store'
            ]   ]);
            /*
                Question Section    
             */
            Route::group(['prefix' =>   'questions' ], function(){
                Route::post('/edit', [
                    'as'    =>  'update.question.super',
                    'uses'  =>  'QuestionsController@update'
                ]);
                Route::post('/destroy/answer',[
                    'as'    =>  'destroy.answer.super',
                    'uses'  =>  'QuestionsController@answerDestroy'
                ]);
                Route::get('/edit', function(){
                    return abort(404);
                });
                Route::post('/destroy', [
                    'as'    =>  'destroy.question.super',
                    'uses'  =>  'QuestionsController@destroy'
                ]); 
            });
            Route::resource('questions', 'QuestionsController', [ 'names' => [
                'index'     =>  'all.questions.super',
                'create'    =>  'create.question.super',
                'store'     =>  'store.question.super',
                'edit'      =>  'edit.question.super'
            ],  'only'      => [
                'index','show','edit','create','store'
            ] ]);

            /*
                Options Section    
             */
            Route::group(['prefix' =>   'options' ], function(){
                Route::post('update', [
                    'as'    =>  'update.option.super',
                    'uses'  =>  'OptionsController@update'
                ]);
                Route::get('update', function(){
                    abort(404);
                });
            });
            Route::resource('options', 'OptionsController', [ 'names' => [
                'index'     =>  'all.options.super',
                'create'    =>  'create.option.super',
                'store'     =>  'store.option.super'
            ],  'only'      => [
                'index','show','edit','create','store'
            ] ]);

            /*
                Media Section
             */
            Route::group(['prefix' => 'media'], function(){
                //other Media Routes
            });
            Route::resource('media', 'MediaController', [ 'names' => [
                'index'     =>  'all.media.super',
                'create'    =>  'create.media.super',
                'store'     =>  'store.media.super',
                'edit'      =>  'edit.media.super',
                'update'    =>  'update.media.super',
                'destroy'   =>  'destroy.media.super'
            ]]);
        });

    });

    //Route::get('', array('https', function(){}));

    
    
});
