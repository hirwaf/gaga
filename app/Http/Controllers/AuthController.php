<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\LoginRepository;
use App\Option;



class AuthController extends Controller
{
    protected $icon,
              $meta,  
              $name,
              $option;
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->option = new Option;
        $this->meta = $this->option->siteinfo('tagline');
        $this->icon = $this->option->siteinfo('icon');
        $this->name = $this->option->siteinfo('sitetitle');

    }

    public function getLogin(LoginRepository $login){
        return view('login',[
            'subTo' => 'login.user',
            'site'  =>  $this->site(),
            'page'  =>  $login
        ]);
    }

    protected function site(){
        return [
            'icon'      =>  $this->icon,
            'meta'      =>  $this->meta,
            'name'      =>  $this->name,
            'logo'      =>  ! empty($this->icon) ? explode('icon.', $this->icon)[1] : null,
            'footer'    =>  [
                'social'    =>  $this->getSocialN(),
                'phones'    =>  $this->getNumer(),
                'logo'      =>  ''
            ]
        ];
    }

    protected function getSocialN(){
        $arr = [];
        if( $this->option->siteinfo('facebook') )
            $arr['facebook'] = $this->option->siteinfo('facebook');
        else $arr['facebook'] = null;
        if( $this->option->siteinfo('twitter') )
            $arr['twitter'] = $this->option->siteinfo('twitter');
        else $arr['twitter'] = null;
        if( $this->option->siteinfo('linkedin') )
            $arr['linkedin'] = $this->option->siteinfo('linkedin');
        else $arr['linkedin'] = null;
        if( $this->option->siteinfo('googleplus') )
            $arr['googleplus'] = $this->option->siteinfo('googleplus');
        else $arr['googleplus'] = null;
        
        return $arr;
    }

    protected function getNumer(){
        $arr = [];
        for($i = 1; $i <= 5; $i++){
            if( count($this->option->siteinfo('phone'.$i)) )
                $arr[] = $this->option->siteinfo('phone'.$i); 
        }

        return $arr;
    }

    public function login(Request $request){
      $email = $request->email;
      $password = $request->password;

      if ( Auth::attempt(['email' => $email, 'password' => $password, 'active' => 1]) ) 
      {
        return Auth::user()->information->firstname;
      }
      elseif ( Auth::attempt(['telephone' => $email, 'password' => $password, 'active' => 1]) )
      {
        return Auth::user()->information->firstname;
      }
      else{
        return redirect()->back()->withErrors(['email' => 'Credentials, you entered are not correct']);
      }
      
    }

}
