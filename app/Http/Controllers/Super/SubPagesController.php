<?php

namespace App\Http\Controllers\Super;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ContactRepository;
use App\Page;
use App\SubPage;
use Auth;
use Session;
use Validator;
use Hash;


class SubPagesController extends Controller
{
    
    public function __construct(){
        $this->middleware('admin');
    }
    
    
    protected function admin(){
        return Auth::guard('admins')->user();
    } 
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ContactRepository $contact)
    {
        return view('auth.layouts.super.addsub',[
            'admin'     =>  $this->admin(),
            'title'     =>  'Add Sub Page',
            'short'     =>  'New',
            'button'    =>  'New Subpage',
            'url'       =>  'store.subpage.super',
            'pages'     =>  Page::where('static',false)->orderBy('name','asc')->get(),
            'contact'   =>  $contact            
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( Hash::check($request->password,$this->admin()->password) ){
            $validator = Validator::make($request->all(),[
                'name'      =>  'required|min:2|unique:pages,name',
                'slug'      =>  'required|min:2|unique:pages,slug',
                'content'   =>  'required|min:10',
                'page'      =>  'required'
            ]);
            if( !$validator->fails() ){
                
                $page = new SubPage;
                $page->page_id = $request->page;
                $page->slug = $request->slug;
                $page->name = ucfirst($request->name);
                $page->content = $request->content;
                $page->valid = true;
                $page->save();
                
                return redirect()->route('all.pages.super');
                
            }
            else{
                Session::put('old',$request->all());
                Session::put('alert-danger', $validator->errors()->first() );
                return redirect()->route('create.subpage.super');
            }
        }
        else{
            Session::put('error', true);
            return redirect()->route('create.subpage.super');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactRepository $contact, $id)
    {
        $subpage = Subpage::findOrFail($id);

        return view('auth.layouts.super.addsub',[
            'admin'     =>  $this->admin(),
            'title'     =>  'Edit " '. $subpage->name .' " ',
            'short'     =>  'Edit',
            'button'    =>  'Update Subpage',
            'edit'      =>  $subpage,
            'url'       =>  'update.subpage.super',
            'pages'     =>  Page::where('static',false)->orderBy('name','asc')->get(),
            'contact'   =>  $contact
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if( Hash::check($request->password,$this->admin()->password) ){
            $page = SubPage::findOrFail($request->id);

            $validator = Validator::make($request->all(),[
                'name'      =>  ($page->name == $request->name) ? '' : 'required|'  . 'min:2|unique:pages,name',
                'slug'      =>  ($page->slug == $request->slug) ? '' : 'required|'  . 'min:2|unique:pages,slug',
                'content'   =>  'required|min:10',
                'page'      =>  'required'
            ]);
            if( !$validator->fails() ){
                
                $page->page_id = $request->page;
                $page->slug = $request->slug;
                $page->name = ucfirst($request->name);
                $page->content = $request->content;

                $page->save();
                
                return redirect()->route('all.pages.super');
                
            }
            else{
                Session::put('old',$request->all());
                Session::put('alert-danger', $validator->errors()->first() );
                return redirect()->route('edit.subpage.super',$request->id);
            }
        }
        else{
            Session::put('error', true);
            return redirect()->route('edit.subpage.super',$request->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
