<?php

namespace App\Http\Controllers\Super;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ContactRepository;
use App\Option;
use Auth;
use Validator;
use Session;
use Hash;


class OptionsController extends Controller
{

    public function __contruct(){
        $this->middleware('admin');
    }

    protected function admin(){
        return Auth::guard('admins')->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ContactRepository $contact)
    {   
        return view('auth.layouts.super.options',[
            'admin'     =>  $this->admin(),
            'title'     =>  'Web Settings',
            'short'     =>  'settings',
            'url'       =>  'update.option.super',
            'button'    =>  'Update Settings',
            'siteinfo'  =>  new Option,
            'contact'   =>  $contact
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        if(Hash::check($request->password,$this->admin()->password)){
            $validator = Validator::make($request->all(),[
                'siteTitle'     =>  'required|min:2|max:10',
                'siteTagLine'   =>  'required|min:2|max:255',
                'siteEmail'     =>  'required|email',
                'phone1'        =>  'integer',
                'phone2'        =>  'integer',
                'phone3'        =>  'integer',
                'phone4'        =>  'integer',
                'phone5'        =>  'integer',
                'map'           =>  'min:5',
                'facebook'      =>  'min:5',
                'twitter'       =>  'min:5',
                'googleplus'    =>  'min:5',
                'linkedin'      =>  'min:5',
                'icon'          =>  'image',
            ]);
            if ( ! $validator->fails() ) {

                if($request->file('icon')){
                    $ex = explode('.', $request->icon->getClientOriginalName());
                    $ex = end($ex);
                    $time__ = time();
                    $filename = 'icon.'.$time__.".".$ex;
                    $image = \Image::make($request->file('icon'));
                    $path  = public_path().'/img/icon/';
                    
                    $image->resize(140,90);
                    $image->save(public_path().'/img/icon/'.$time__.".".$ex);

                    $image->resize(51,32);
                    $image->save($path.$filename);
                    $icon = $filename;
                }
                else{
                    $icon = Option::where('name','icon')->get()->first()->value;
                }
                
                Option::where('name','sitetitle')->update(['value' => ucfirst($request->siteTitle) ]);
                Option::where('name','tagline')->update(['value'=> ucfirst($request->siteTagLine) ]);
                Option::where('name','email')->update(['value' => $request->siteEmail ]);

                Option::where('name','phone1')->update(['value' => ( strlen($request->phone1) == 12  ) ? $request->phone1 : Null ]);
                Option::where('name','phone2')->update(['value' => ( strlen($request->phone2) == 12  ) ? $request->phone2 : Null ]);
                Option::where('name','phone3')->update(['value' => ( strlen($request->phone3) == 12  ) ? $request->phone3 : Null ]);
                Option::where('name','phone4')->update(['value' => ( strlen($request->phone4) == 12  ) ? $request->phone4 : Null ]);
                Option::where('name','phone5')->update(['value' => ( strlen($request->phone5) == 12  ) ? $request->phone5 : Null ]);

                Option::where('name','map')->update(['value' => ($request->map != "" ) ? $request->map : Null ]);

                Option::where('name','facebook')->update(['value'   => ($request->facebook != "" )  ? $request->facebook : Null ]);
                Option::where('name','twitter')->update(['value'    => ($request->twitter != "" )   ? $request->twitter : Null ]);
                Option::where('name','googleplus')->update(['value' => ($request->googleplus != "" ) ? $request->googleplus : Null ]);
                Option::where('name','linkedin')->update(['value'   => ($request->linkedin != "" )  ? $request->linkedin : Null ]);

                Option::where('name','icon')->update(['value' => $icon ]);

                return redirect()->route('all.options.super');

            }
            else{
                Session::put('alert-danger', $validator->errors()->first() );
                return redirect()->route('all.options.super');   
            }
        }
        else{
            Session::put('error', true);
            return redirect()->route('all.options.super');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
