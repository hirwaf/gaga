<?php

namespace App\Http\Controllers\Super;

use Illuminate\Http\Request;
use Auth;
use Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ContactRepository;
use Validator;
use App\Admin;
use App\Place;
use App\Location;
use App\Message;
class SuperController extends Controller
{
    //
    public $admin;
    
    public function __construct(){
        $this->middleware('admin');
        
        $this->admin = Auth::guard('admins')->user();
    }
    
    public function getDashboard(ContactRepository $contact){
        
        return view('auth.layouts.super.dashboard',[
            'admin'     =>  $this->admin,
            'contact'   =>  $contact
        ]);
        
    }
    
    public function getProfile(ContactRepository $contact){
        return view('auth.layouts.super.profile',[
            'admin' =>  $this->admin,
            'contact'   =>  $contact
        ]);
    }
    
    public function updateProfile(Request $request){
        $password = $this->admin->password;
        if(Hash::check($request->input('password'),$password)){
            
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'email' =>  'required|email'.($this->admin->email == $request->input('email'))? '' : '|unique:admins,email',
                'phone' =>  'required|min:12'.($this->admin->email == $request->input('email'))? '' : '|unique:admins,phone'
            ]);
            if($request->has('new_password') AND $request->has('new_password_confirmation') ){
                $inp = [
                    'new_password'               =>  $request->get('new_password'),
                    'new_password_confirmation'  =>  $request->get('new_password_confirmation')
                ];
                $validator = Validator::make($inp,[
                    'new_password'   =>  'required|confirmed|min:8'
                ]);
            }
            if( !$validator->fails() ){
                
                $admin = Admin::findOrFail($this->admin->id);
                $admin->name = $request->input('name');
                $admin->email = $request->input('email');
                $admin->phone = $request->input('phone');
                if(isset($inp)){
                    $admin->password = bcrypt($request->input('new_password'));
                }
                $admin->save();

                \Session::put('alert-success','Your profile has successfuly update .');
                return redirect()->route('profile.super');
            }
            else{
                \Session::put('alert-danger',$validator->errors()->first());
                return redirect()->route('profile.super');
            }
        }
        else{
            \Session::put('error', true);
            return redirect()->route('profile.super');
        }
    }

    public function sendEmail(Request $request)
    {
        $email = \Mail::send('auth.emails.person', [ 'request'=> $request ] , function($message) use ($request) {
            $message->to($request->email, $request->name);
            $message->subject($request->message);
        });

        if ($email == 1)
            session(['flash_' => 'Message Sussefuly Sended']);
        else
            session(['flash_' => 'Fail to send massage try again later']);
        
        return redirect()->back();
    }

    public function markAsRead(Request $request)
    {
        $id = $request->id;
        $d = [
            'seen'  =>  true,
            'read'  =>  true
        ];

        if ($id == 'a')
            Message::query()->update($d);         
        else
            Message::find($id)->update($d);
    }

}
