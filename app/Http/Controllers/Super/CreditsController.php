<?php

namespace App\Http\Controllers\Super;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Repositories\ContactRepository;
use Auth;
use Session;
use Hash;
use Validator;
use App\Credit;
use App\Course;
use App\Media;
use Storage;

class CreditsController extends Controller
{
    
    
    public function __construct(){
        $this->middleware('admin');
    }
    
    protected function admin(){
        return Auth::guard('admins')->user();
    }
    
    public function read($title){
        $credit = Credit::where('title', $title)->first();
        $url = $credit->media->url;
        $content = Storage::url('notes/'.$url).".pdf";
        return response($content);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ContactRepository $contact)
    {
        return view('auth.layouts.super.credits', [
            'admin'     =>  $this->admin(),
            'credits'   =>  Credit::orderBy('created_at', 'desc')->get(),
            'contact'   =>  $contact
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ContactRepository $contact)
    {
        
       return view('auth.layouts.super.addcredit',[
            'admin'     =>  $this->admin(),
            'title'     =>  "Add New Credit",
            'url'       =>  'store.credit.super',
            'button'    =>  'Add Credit',
            'short'     =>  'New',
            'courses'   =>  Course::all(['id','course']),
            'contact'   =>  $contact
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Hash::check($request->password,$this->admin()->password)){
            $validator = Validator::make($request->all(),[
                'course'        =>  'required|integer|exists:courses,id',
                'creditTitle'   =>  'required|min:2|unique:credits,title',
                'summary'       =>  'min:10|max:255'
            ]);
            if( !$validator->fails() ){
                if($request->hasFile('content')){
                    $file = $request->content->getClientOriginalName();
                    $ex = explode('.',$file);
                    $ex = strtolower(end($ex));
                    if($ex == 'pdf'){
                        $time = collect(str_split(time()));
                        $rand = $time->random(4)->all(); 
                        $rand = join($rand);
                        $url = date('Y_m_d',time()) . "_" . $rand . "_" . str_replace(" ","_",$request->creditTitle);
                        $pathD = public_path()."/files/";
                        $store = $request->file('content')->move($pathD, $url.".".$ex);
                        
                        if($store){
                            $media = new Media;
                            
                            $media->title = $request->creditTitle;
                            $media->type = "pdf";
                            $media->url = $url;
                            $media->valid = true;
                            $media->save();
                            
                            $media_id = $media->where('title',$request->creditTitle)->first()->id;
                            
                            $credit = new Credit;
                    
                            $credit->title = ucfirst($request->creditTitle);
                            $credit->summary = $request->summary;
                            $credit->course_id = $request->course;
                            $credit->media_id = $media_id;
                            $credit->active = true;
                            $credit->save();
                            
                            Session::put('alert-success', 'Successfuly created .' );
                            return redirect()->route('create.credit.super');
                        }
                        else{
                            Session::put('old',$request->all());
                            Session::put('alert-warning', 'Upload fail try again later ' );
                            return redirect()->route('create.credit.super');        
                        }
                    }
                    else{
                        Session::put('old',$request->all());
                        Session::put('alert-danger', 'Content must be a pdf file' );
                        return redirect()->route('create.credit.super');
                    }
                }
                else{
                    Session::put('old',$request->all());
                    Session::put('alert-danger', 'Content is required' );
                    return redirect()->route('create.credit.super');    
                }
            }
            else{
                Session::put('old',$request->all());
                Session::put('alert-danger', $validator->errors()->first() );
                return redirect()->route('create.credit.super');
            }
        }
        else{
            Session::put('error', true);
            return redirect()->route('create.credit.super');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactRepository $contact,$id)
    {
        $credit = Credit::findOrFail($id);
        return view('auth.layouts.super.addcredit',[
            'admin'     =>  $this->admin(),
            'edit'      =>  $credit,
            'title'     =>  "Edit \"".$credit->title."\"",
            'url'       =>  'update.credit.super',
            'button'    =>  'Edit Credit',
            'short'     =>  'New',
            'courses'   =>  Course::all(['id','course']),
            'contact'   =>  $contact
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(Hash::check($request->password,$this->admin()->password)){
            $credit = Credit::find($request->id);
            $validator = Validator::make($request->all(),[
                'creditTitle'   =>  'required|min:2'.( $credit->title != $request->creditTitle ) ? "" : "|unique:credits,title",
                'summary'       =>  'min:10|max:255',
                'course'        =>  'integer|exists:courses,id'
            ]);
            if(! $validator->fails()){
                if($request->hasFile('content')){
                    $file = $request->content->getClientOriginalName();
                    $ex = explode('.',$file);
                    $ex = strtolower(end($ex));
                    if($ex == 'pdf'){
                        $time = collect(str_split(time()));
                        $rand = $time->random(4)->all(); 
                        $rand = join($rand);
                        $url = date('Y_m_d',time()) . "_" . $rand . "_" . str_replace(" ","_",$request->creditTitle);
                        $store = Storage::put('notes/'.$url,file_get_contents($request->file('content')->getRealPath()));
                        
                        $media = Media::find($credit->media_id);
                        $urlD = $media->url;
                        Storage::delete('notes/'.$urlD);
                        $media->title = $request->creditTitle;
                        $media->url = $url;
                        $media->save();
                    }
                    else{
                        Session::put('old',$request->all());
                        Session::put('alert-danger', 'Content must be a pdf file');
                        return redirect()->route('edit.credit.super', $request->id);
                    }
                }
                $credit->title = $request->creditTitle;
                $credit->summary = $request->summary;
                $credit->course_id = $request->course;
                $credit->save();
                return redirect()->route('all.credits.super');
            }
            else{
                Session::put('old',$request->all());
                Session::put('alert-danger', $validator->errors()->first() );
                return redirect()->route('edit.credit.super',$request->id);
            }
        }
        else{
            Session::put('error', true);
            return redirect()->route('edit.credit.super',$request->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(Hash::check($request->password,$this->admin()->password)){
            $credit = Credit::where('id',$request->credit)->first();
            $media = Media::where('id',$credit->media_id);
            $url = $media->first()->url;
            $media->delete();
            Storage::delete('notes/'.$url);
            
            return redirect()->route('all.credits.super');
        }
        else{
            Session::put('error', true);
            return redirect()->route('all.credits.super');
        }
    }
}
