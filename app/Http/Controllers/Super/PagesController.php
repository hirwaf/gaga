<?php

namespace App\Http\Controllers\Super;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ContactRepository;

use Auth;
use Hash;
use Session;
use Validator;
use App\Page;
use App\SubPage;
use App\Option;
use App\Menu;
use App\Admin;
use Image;

class PagesController extends Controller
{

    public function __construct(){
        $this->middleware('admin');
    }

    public function admin(){
        return Auth::guard('admins')->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ContactRepository $contact)
    {
        return view('auth.layouts.super.pages', [
            'admin'     =>  $this->admin(),
            'pages'      =>  Page::orderBy('static','desc')->get(),
            'contact'	=> $contact
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ContactRepository $contact)
    {
        return view('auth.layouts.super.addpage', [
            'admin'     =>  $this->admin(),
            'title'     =>  "Add New Page",
            'short'     =>  'New',
            'url'       =>  'store.page.super',
            'button'    =>  'Add New Page',
            'contact'	=> $contact
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( Hash::check($request->password,$this->admin()->password) ){
            $validator = Validator::make($request->all(),[
                'name'      =>  'required|min:2|unique:pages,name',
                'slug'      =>  'required|min:2|unique:pages,slug',
                'content'   =>  ($request->isstatic == 1)? '' : 'required|' . 'min:10'
            ]);
            if( !$validator->fails() ){

                $page = Page::create([
                    'slug'      => $request->slug,
                    'name'      => ucfirst($request->name),
                    'static'    => $request->isstatic == 1? true : false,
                    'content'   => $request->content
                ]);
                $menus = Menu::count() + 1;
                Menu::create([
                    'page_id'   =>  $page->id,
                    'order'     =>  $menus,
                    'active'    =>  false
                ]);

                return redirect()->route('all.pages.super');

            }
            else{
                Session::put('old',$request->all());
                Session::put('alert-danger', $validator->errors()->first() );
                return redirect()->route('create.page.super');
            }
        }
        else{
            Session::put('error', true);
            return redirect()->route('create.page.super');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactRepository $contact, $id)
    {
        $page = Page::findOrFail($id);
        if( $page->static ){
            return abort(404);
        }
        session(['_id_' => $id]);
        return view('auth.layouts.super.addpage', [
            'admin'     =>  $this->admin(),
            'title'     =>  "Edit \" ". ucfirst($page->name) ." \" ",
            'edit'      =>  $page,
            'short'     =>  'Edit',
            'url'       =>  'update.page.super',
            'button'    =>  'Update Page',
            'contact'	=> $contact,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if( Hash::check($request->password,$this->admin()->password) ){
            $page = Page::findOrFail($request->id);
            $validator = Validator::make($request->all(),[
                'name'      => ($page->name == $request->name) ? '' : 'required|'  .'min:2|unique:pages,name',
                'slug'      => ($page->slug == $request->slug) ? '' : 'required|'  . 'min:2|unique:pages,slug',
                'content'   =>  'min:10'
            ]);
            if( !$validator->fails() ){

                if($request->has('gallery')){
                    $inner = is_array( json_decode($page->content,true) ) ? true : false;
                    $images = [];

                    $i = ( count(json_decode($page->content,true)) > 0 )? count( json_decode($page->content,true) ) + 1 : 1;

                    foreach ($request->file('images') as $value) {
                        $ex = $value->getClientOriginalExtension();
                        $filename = 'gallery-'.$i.time().'.'.$ex;
                        $path = public_path('/img/gallery/');

                        $image = Image::make($value);

                        $image->resize(600,450);
                        $image->save($path.$filename);
                        $images[$i] = $filename;
                        $i++;
                    }
                    if( count(json_decode($page->content,true)) <= 0 )
                        $content = json_encode($images);
                    else{
                        $old = json_decode($page->content,true);
                        $images = array_merge($old,$images);
                        $content = json_encode($images);
                    }
                }
                elseif(! $request->has('gallery') )
                    $content = $request->content;


                $page->content = $content;

                $page->save();

                return redirect()->route('all.pages.super');

            }
            else{
                Session::put('old',$request->all());
                Session::put('alert-danger', $validator->errors()->first() );
                return redirect()->route('edit.page.super',$request->id);
            }
        }
        else{
            Session::put('error', true);
            return redirect()->route('edit.page.super',$request->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $page = Page::findOrFail($request->page);
        $page->delete();
        return redirect()->route('all.pages.super');
    }

    public function editstatic(ContactRepository $contact,$id)
    {
        $page = Page::findOrFail($id);
        if( ! $page->static )
            return abort(404);

        return view('auth.layouts.super.staticpage', [
            'admin'     =>  $this->admin(),
            'title'     =>  "Edit \" ". ucfirst($page->name) ." \" ",
            'edit'      =>  $page,
            'short'     =>  'Edit',
            'url'       =>  'update.page.super',
            'button'    =>  'Update Page'
        ]);

    }

    public function getMenu(ContactRepository $contact)
    {
        return view('auth.layouts.super.sitemenu', [
            'admin'     =>  $this->admin(),
            'menus'     =>  Menu::orderBy('order','asc')->get(),
            'contact'   =>  $contact
        ]);
    }

    public function orderMenu(Request $request)
    {
        $menu = Menu::findOrFail($request->menu);
        $whohas = Menu::where('order',$request->order)->first();
        if($whohas){
            $whohas->order = $menu->order;
            $whohas->save();
        }
        $menu->order = $request->order;
        $menu->save();

        return redirect()->route('menu.page.super');
    }

    public function statusMenu(Request $request)
    {

        $onauth = $request->onauth == 'y' ? true : false;
        $onlyauth = $request->onlyauth == 'y' ? true : false;

        $menu = Menu::findOrFail($request->menu);
        $menu->active = $request->active;
        $menu->auth = $onauth;
        $menu->only = $onlyauth;
        $menu->save();

        return redirect()->route('menu.page.super');
    }



}
