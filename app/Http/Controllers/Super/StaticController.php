<?php

namespace App\Http\Controllers\Super;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ContactRepository;
use App\Page;
use App\PageContent;
use Auth;
use Session;
use Hash;
use Validator;

class StaticController extends Controller
{

	public function __construct(){
		$this->middleware('admin');
	}

	public function admin()
	{
        return Auth::guard('admins')->user();
    }

	public function update(Request $request)
	{
		if( Hash::check($request->password,$this->admin()->password) ){
            $page = Page::findOrFail($request->id);
            $validator = Validator::make($request->all(),[
                'name'      => ($page->name == $request->name) ? '' : 'required|'  .'min:2|unique:pages,name',
                'slug'      => ($page->slug == $request->slug) ? '' : 'required|'  . 'min:2|unique:pages,slug',
                'content'   =>  'required|min:10'
            ]);
            if( !$validator->fails() ){

                $media = [];
                $pageContent = PageContent::where('page_id',$request->id)->first();
                $mediaP = $pageContent->count()  ? json_decode($pageContent->media, true) : [];

            	if ($request->hasslide) {
            		if( count($mediaP) <= 5 ) {
                        foreach ($request->file('image') as $key => $value) {
                            if($value){
                                $file = $value;
                                $ex = $value->getClientOriginalExtension();
                                $time__ = time();
                                $filename = 'slide.'.$key.'.'.$time__.'.'.$ex;
                                $path = public_path().'/img/slides/';

                                $image = \Image::make($file);

                                $image->resize(1140,450);
                                $image->save($path.$filename);

                                unset($image);

                                $media[$key] = [
                                    'image'     =>  $filename,
                                    'title'     =>  $request->input('title')[$key],
                                    'desc'      =>  $request->input('desc')[$key]
                                ];
                            }
                        }
                        $merged = array_merge($mediaP, $media);
                    }
                    else{
                        foreach ($request->file('image') as $key => $value) {
                            if($value){
                                $file = $value;
                                $ex = $value->getClientOriginalExtension();
                                $time__ = time();
                                $filename = 'slide.'.$key.'.'.$time__.'.'.$ex;
                                $path = public_path().'/img/slides/';

                                $image = \Image::make($file);

                                $image->resize(1140,450);
                                $image->save($path.$filename);
                                unset($image);

                                $mediaP[$key] = [];
                                $mediaP[$key] = [
                                    'image'     =>  $filename,
                                    'title'     =>  $request->input('title')[$key],
                                    'desc'      =>  $request->input('desc')[$key]
                                ];
                            }
                        }
                        $merged = $mediaP;
                    }
                }

                $media_ = json_encode($merged, JSON_FORCE_OBJECT);

                $new = new PageContent();
            	if( $pageContent )
                    $new = $pageContent;

                if( ! $pageContent )
        		  $new->page_id = $request->id;

                $new->media = $media_;
        		$new->meta = null;
        		$new->behevior = 'slide';
        		$new->text = $request->content;
        		$new->isvalid = true;

        		$new->save();

                //$page->slug = $request->slug;
                //$page->name = ucfirst($request->name);
                $page->content = null;

                $page->save();

                return redirect()->route('all.pages.super');

            }
            else{
                Session::put('old',$request->all());
                Session::put('alert-danger', $validator->errors()->first() );
                return redirect(route('static.page.super', $request->id));
                // return redirect('/static/'.$request->id)->route('static.page.super', $request->id);
            }
        }
        else{
            Session::put('error', true);
            return redirect(route('static.page.super', $request->id));
            // return redirect('/static/'.$request->id);
        }
	}

	public function edit(ContactRepository $contact, $id)
	{
		$page = Page::findOrFail($id);
        if( ! $page->static )
            return abort(404);

        return view('auth.layouts.super.staticpage', [
            'admin'     =>  $this->admin(),
            'title'     =>  "Edit \" ". ucfirst($page->name) ." \" ",
            'edit'      =>  $page,
            'short'     =>  'Edit',
            'url'       =>  'static.page.super',
            'button'    =>  'Update Page',
            'contact'   =>  $contact
        ]);
	}


}
