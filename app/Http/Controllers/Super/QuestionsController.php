<?php

namespace App\Http\Controllers\Super;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ContactRepository;
use Hash;
use Session;
use Validator;
use Auth;
use App\Question;
use App\Answer;
use App\Credit;

class QuestionsController extends Controller
{
    public function __contruct(){
        $this->middleware('admin');
    }
    
    protected function admin(){
        return Auth::guard('admins')->user();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( ContactRepository $contact )
    {
        return view('auth.layouts.super.questions', [
            'admin'     =>  $this->admin(),
            'questions' =>  Question::orderBy('created_at', 'desc')->get(),
            'title'     =>  "All Questions",
            'short'     =>  "All",
            'contact'   =>  $contact
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( ContactRepository $contact )
    {
        return view('auth.layouts.super.addquestion', [
            'admin'     =>  $this->admin(),
            'credits'   =>  Credit::all('id','title'),
            'title'     =>  "Add New Question",
            'url'       =>  'store.question.super',
            'button'    =>  'Add Question',
            'short'     =>  'New',
            'toedit'    =>  false,
            'contact'   =>  $contact
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        if(Hash::check($request->password,$this->admin()->password)){
            $validator = Validator::make($request->all(),[
                'correct'   =>  'required',
                'question'  =>  'required|min:1|unique:questions,question',
                'answer_1'  =>  'required|min:1',
                'answer_2'  =>  'required|min:1',
                'answer_3'  =>  'min:1',
                'answer_4'  =>  'min:1',
                'answer_5'  =>  'min:1'
            ]);
            if( ! $validator->fails()){
                if( trim($request->input($request->correct)) != "" ){
                    $question = new Question;
                    $question->credit_id = $request->credit;
                    $question->question = trim($request->question);
                    $question->marks = 5;
                    $question->time = 5;
                    $question->save();
                    $que_id = $question::where('question',$request->question)
                                         ->where('credit_id',$request->credit)
                                         ->first()->id;
                    for($i = 1; $i <= 5; $i++ ){
                        if( trim($request->input('answer_'.$i)) != "" ){
                            $answer = new Answer;
                            $answer->question_id = $que_id;
                            $answer->answer = $request->input('answer_'.$i);
                            $answer->iscorrect = ( $request->correct == "answer_".$i ) ? true : false;
                            $answer->save();
                        }
                    }
                    
                    return redirect()->route('all.questions.super');
                    
                }
                else{
                    Session::put('alert-danger', 'You setted a YES to an empty answer field !');
                    Session::put('old', $request->all());
                    return redirect()->route('create.question.super');
                }
            }
            else{
                Session::put('alert-danger', $validator->errors()->first());
                Session::put('old', $request->all());
                return redirect()->route('create.question.super');
            }
        }
         else{
            Session::put('error', true);
            Session::put('old',$request->all());
            return redirect()->route('create.question.super');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactRepository $contact, $id)
    {
        $question = Question::findOrFail($id);
        
        return view('auth.layouts.super.addquestion', [
            'admin'     =>  $this->admin(),
            'credits'   =>  Credit::all('id','title'),
            'title'     =>  "Edit \" ". $question->question ."  \"  ",
            'edit'      =>  $question,
            'url'       =>  'update.question.super',
            'button'    =>  'Update',
            'short'     =>  'Edit',
            'toedit'    =>  true,
            'contact'   =>  $contact
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(Hash::check($request->password,$this->admin()->password)){
            $question = Question::find($request->id);
            
            $validator = Validator::make($request->all(),[
                'correct'   =>  'required',
                'question'  =>  'required|min:1'.( $request->question == $question->question )? '' : '|unique:questions,question'  ,
                'answer_1'  =>  'required|min:1',
                'answer_2'  =>  'required|min:1',
                'answer_3'  =>  'min:1',
                'answer_4'  =>  'min:1',
                'answer_5'  =>  'min:1'
            ]);
            if( ! $validator->fails() ){
                if( trim($request->input($request->correct)) != "" ){
                    $question = Question::find($request->id);
                    
                    $question->credit_id = $request->credit;
                    $question->question = trim($request->question);
                    $question->save();
                    
                    $answers = Answer::where('question_id',$request->id)->get();
                    $i = 1;
                    foreach($answers as $ans ){
                        $answer = Answer::find($ans->id);
                        if( trim($request->input('answer_'.$i)) != "" ){
                            $answer->answer = $request->input('answer_'.$i);
                            $answer->iscorrect = ( $request->correct == "answer_".$i ) ? true : false;
                            $answer->save();
                        }
                        $i++;
                    }
                    
                    return redirect()->route('all.questions.super');
                    
                }
                else{
                    Session::put('alert-danger', 'You setted a YES to an empty answer field !');
                    Session::put('old', $request->all());
                    return redirect()->route('create.question.super');
                }
            }
            else{
                Session::put('alert-danger', $validator->errors()->first());
                Session::put('old', $request->all());
                return redirect()->route('edit.question.super',$request->id);
            }
        }
        else{
             Session::put('error', true);
            Session::put('old',$request->all());
            return redirect()->route('edit.question.super',$request->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $question = Question::findOrFail($request->question);
        $question->delete();
        return redirect()->route('all.questions.super');
    }
    
    public function answerDestroy(Request $request){
        $answer = Answer::findOrFail($request->question);
        $answer->delete();
        return redirect()->route('all.questions.super');
    }
}
