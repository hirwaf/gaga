<?php

namespace App\Http\Controllers\Super;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ContactRepository;
use App\Repositories\UserRepository;
use Auth;
use App\User;
use App\Information;
use Session;
use Validator;


class UsersController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('admin');
    }

    protected function admin()
    {
        return Auth::guard('admins')->user();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ContactRepository $contact)
    {
        return view('auth.layouts.super.alluser',[
            'admin' =>  $this->admin(),
            'users' =>  Information::orderBy('created_at', 'desc')->get(),
            'title' =>  'All',
            'contact'   =>  $contact,
        ]);
    }

    public function getAllowed(ContactRepository $contact)
    {
        return view('auth.layouts.super.alluser',[
            'admin' =>  $this->admin(),
            'users' =>  Information::orderBy('created_at', 'desc')->where('approved',true)->get(),
            'title' =>  'Allowed',
            'contact'   =>  $contact,
        ]);
    }

    public function getNotAllowed(ContactRepository $contact)
    {
        return view('auth.layouts.super.alluser',[
            'admin' =>  $this->admin(),
            'users' =>  Information::orderBy('created_at', 'desc')->where('approved',false)->get(),
            'title' =>  'Not yet Allowed',
            'contact'   =>  $contact,
        ]);
    }

    public function allow(Request $request){
        $info = Information::findOrFail($request->client);
        $info->approved = 1;
        $info->save();

        return redirect()->back();
    }

    public function reverseApproval(Request $request) {
        
        $userRepo = new UserRepository();
        $userRepo->reverseApproval($request->input('__id__'));

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ContactRepository $contact)
    {
        return view('auth.layouts.super.adduser',[
            'admin'     =>  $this->admin(),
            'contact'   =>  $contact,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(\Hash::check($request->password,$this->admin()->password)){
            $id = $request->name;
            $employee = Employee::find($id);
            $password = trim($request->makepassword);
            $users = User::all();
            $exists = false;
            
            foreach ($users as $user) {
                if( $employee->email == $user->email ){
                    $exists = true;
                    break;
                }
            }

            if($exists){
                Session::put('alert-warning', '" '.ucfirst($employee->names).' " Already Added !' );
                return redirect()->route('add.user.super');
            }
            elseif( $password != "" AND isset($password) AND strlen($password) >= 8 ){
                $user = new User();
                $user->email = $employee->email;
                $user->telephone = $employee->phone;
                $user->employee_id = $id;
                $user->password = bcrypt($password);
                if($user->save()){
                    Session::put('alert-success', ucfirst( $request->name ) . ' was successfuly added.' );
                    Session::put('userS',[
                        'name'      =>  $employee->names,
                        'email'     =>  $employee->email,
                        'phone'     =>  $employee->phone,
                        'password'  =>  $password
                    ]);
                    return redirect()->route('add.user.super');
                }
                else{
                    Session::put('old',$request->all());  
                    Session::put('alert-warning', 'Server error ! Try again later. ' );
                    return redirect()->route('add.user.super');
                }
            }else{
                Session::put('old',$request->all());
                Session::put('alert-danger', 'Password must be 8 characters or more ! ' );
                return redirect()->route('add.user.super');
            }

        }else{
            \Session::put('error', true);
            return redirect()->route('add.user.super');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ContactRepository $contact, $id)
    {
        $user = Information::findOrFail($id);
        $repo = new UserRepository($id);
        return view('auth.layouts.super.viewuser',[
            'admin'     =>  $this->admin(),
            'user'      =>  $user,
            'userrepo'  =>  $repo,
            'contact'   =>  $contact,
        ]);        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(\Hash::check($request->password,$this->admin()->password)){
            $user = User::find($request->user);
            $what = $request->what;
            $user->allowed = $what;
            $user->save();
            return redirect()->route('all.user.super');
        }else{
            \Session::put('error', true);
            return redirect()->route('all.user.super');
        }
    }
}
