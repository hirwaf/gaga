<?php

namespace App\Http\Controllers\Super;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ContactRepository;
use Auth;
use Hash;
use Session;
use Validator;
use App\Course;

class CoursesController extends Controller
{
    public function __construct(){
        $this->middleware('admin');
    }
    
    protected function admin(){
        return Auth::guard('admins')->user();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ContactRepository $contact)
    {
        return view('auth.layouts.super.courses',[
            'admin'     =>  $this->admin(),
            'courses'    =>  Course::orderBy('created_at', 'desc')->get(),
            'contact'   =>  $contact
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ContactRepository $contact)
    {
        return view('auth.layouts.super.addcourse',[
            'admin'     =>  $this->admin(),
            'title'     =>  "Add New Course",
            'url'       =>  'store.course.super',
            'button'    =>  'Add Course',
            'short'     =>  'New',
            'contact'   =>  $contact
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Hash::check($request->password,$this->admin()->password)){
            $validator = Validator::make($request->all(),[
                'courseTitle'   =>  'required|unique:courses,course|min:2',
                'summary'       =>  'min:10|max:255'
            ]);
            if(! $validator->fails()){
                $course = new Course;
                
                $course->course = $request->courseTitle;
                $course->summary = $request->summary;
                $course->valid = 1;
                
                if($course->save()){
                    Session::put('alert-success', ucfirst( $request->name ) . ' was successfuly added to courses' );
                    return redirect()->route('create.course.super');
                }
                else{
                    Session::put('old',$request->all());  
                    Session::put('alert-warning', 'Server error ! Try again later. ' );
                    return redirect()->route('create.course.super');
                }
            }
            else{
                Session::put('old',$request->all());
                Session::put('alert-danger', $validator->errors()->first() );
                return redirect()->route('create.course.super');
            }
        }
        else{
            Session::put('error', true);
            return redirect()->route('create.course.super');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactRepository $contact, $id)
    {
        $course = Course::findOrFail($id);
        return view('auth.layouts.super.addcourse', [
            'admin'     =>  $this->admin(),
            'edit'      =>  $course,
            'title'     =>  "Edit \"" . $course->course . "\" ",
            'url'       =>  'update.course.super',
            'button'    =>  'Update Course',
            'short'     =>  'Edit',
            'contact'   =>  $contact
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(Hash::check($request->password,$this->admin()->password)){
            $course = Course::find($request->id);
            $validator = Validator::make($request->all(),[
                'courseTitle'   =>  'required|min:2'.( $course->course != $request->courseTitle ) ? "" : "|unique:courses,course",
                'summary'       =>  'min:10|max:255'
            ]);
            if(! $validator->fails()){
                
                
                $course->course = $request->courseTitle;
                $course->summary = $request->summary;
                $course->save();
                
                return redirect()->route('all.courses.super');
            }
            else{
                Session::put('old',$request->all());
                Session::put('alert-danger', $validator->errors()->first() );
                return redirect()->route('edit.course.super',$request->id);
            }
        }
        else{
            Session::put('error', true);
            return redirect()->route('edit.course.super',$request->id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(Hash::check($request->password,$this->admin()->password)){
            $course = Course::where('id',$request->course);
            $course->delete();
            return redirect()->route('all.courses.super');
        }
        else{
            Session::put('error', true);
            return redirect()->route('all.courses.super');
        }
    }
}
