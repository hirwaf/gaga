<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\ContactRequest;
use App\Repositories\ContactRepository;
use App\Page;
use App\Option;



class ContactUsController extends Controller
{
    
    protected $icon,
              $meta,  
              $name,
              $option;

    public function __construct()
    {
        $this->middleware('web');
        $this->option = new Option;
        $this->meta = $this->option->siteinfo('tagline');
        $this->icon = $this->option->siteinfo('icon');
        $this->name = $this->option->siteinfo('sitetitle');
    }          
	
    public function getContactUs()
    {
    	return view('contact-us',[
            //'page'  =>  $gallery,
            'site'  =>  $this->site()
        ]);	
    }

    public function postContactUs(ContactRepository $repo, Request $request)
    {
    	$repo->store($request);

        if ( $repo->pass_ ) {
            session([ 'success' => "Your Message sent, Please keep checking your email for feedback ." ]);
            return redirect(route('contact.us.web'));
        }
        else{
            session(['errors' => $repo->errors_ ]);
            return redirect(route('contact.us.web'))->withInput();
        }
    }

    protected function site(){
        return [
            'icon'      =>  $this->icon,
            'meta'      =>  $this->meta,
            'name'      =>  $this->name,
            'logo'      =>  ! empty($this->icon) ? explode('icon.', $this->icon)[1] : null,
            'footer'    =>  [
                'social'    =>  $this->getSocialN(),
                'phones'    =>  $this->getNumer(),
                'logo'      =>  ''
            ]
        ];
    }

    protected function getSocialN(){
        $arr = [];
        if( $this->option->siteinfo('facebook') )
            $arr['facebook'] = $this->option->siteinfo('facebook');
        else $arr['facebook'] = null;
        if( $this->option->siteinfo('twitter') )
            $arr['twitter'] = $this->option->siteinfo('twitter');
        else $arr['twitter'] = null;
        if( $this->option->siteinfo('linkedin') )
            $arr['linkedin'] = $this->option->siteinfo('linkedin');
        else $arr['linkedin'] = null;
        if( $this->option->siteinfo('googleplus') )
            $arr['googleplus'] = $this->option->siteinfo('googleplus');
        else $arr['googleplus'] = null;
        
        return $arr;
    }

    protected function getNumer(){
        $arr = [];
        for($i = 1; $i <= 5; $i++){
            if( count($this->option->siteinfo('phone'.$i)) )
                $arr[] = $this->option->siteinfo('phone'.$i); 
        }

        return $arr;
    }


}
