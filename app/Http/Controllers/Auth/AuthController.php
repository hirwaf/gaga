<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\User;
use Validator;
use App\Repositories\LoginRepository;
use App\Option;
use Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
     protected $redirectTo = "/client/dashboard";
     protected $guard = 'web';
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    protected $icon,
              $meta,  
              $name,
              $option;
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
        $this->option = new Option;
        $this->meta = $this->option->siteinfo('tagline');
        $this->icon = $this->option->siteinfo('icon');
        $this->name = $this->option->siteinfo('sitetitle');

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            #'name' => 'required|max:255',
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    
    public function getLogin(LoginRepository $login){
        return view('login',[
            'subTo' => 'login.user',
            'site'  =>  $this->site(),
            'page'  =>  $login
        ]);
    }
    protected function site(){
        return [
            'icon'      =>  $this->icon,
            'meta'      =>  $this->meta,
            'name'      =>  $this->name,
            'logo'      =>  ! empty($this->icon) ? explode('icon.', $this->icon)[1] : null,
            'footer'    =>  [
                'social'    =>  $this->getSocialN(),
                'phones'    =>  $this->getNumer(),
                'logo'      =>  ''
            ]
        ];
    }

    protected function getSocialN(){
        $arr = [];
        if( $this->option->siteinfo('facebook') )
            $arr['facebook'] = $this->option->siteinfo('facebook');
        else $arr['facebook'] = null;
        if( $this->option->siteinfo('twitter') )
            $arr['twitter'] = $this->option->siteinfo('twitter');
        else $arr['twitter'] = null;
        if( $this->option->siteinfo('linkedin') )
            $arr['linkedin'] = $this->option->siteinfo('linkedin');
        else $arr['linkedin'] = null;
        if( $this->option->siteinfo('googleplus') )
            $arr['googleplus'] = $this->option->siteinfo('googleplus');
        else $arr['googleplus'] = null;
        
        return $arr;
    }

    protected function getNumer(){
        $arr = [];
        for($i = 1; $i <= 5; $i++){
            if( count($this->option->siteinfo('phone'.$i)) )
                $arr[] = $this->option->siteinfo('phone'.$i); 
        }

        return $arr;
    }

    public function login(Request $request){
      $email = $request->email;
      $password = $request->password;

      if ( Auth::guard('web')->attempt(['email' => $email, 'password' => $password]) ) 
      {
        return redirect()->intended($this->redirectTo);
      }
      elseif ( Auth::attempt(['telephone' => $email, 'password' => $password]) )
      {
        return redirect()->intended($this->redirectTo);
      }
      else{
        session(['feedback'=>'Invalid Credentials']);
        return redirect()->back();
      }
      
    }
}
