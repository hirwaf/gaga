<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Page;
use App\Option;
use App\PageContent;
use App\Credit;
use App\Course;
use App\Student;
use App\Answer;
use App\Quiz;
use Route;
use App\Repositories\StatisticRepository;

class QuizController extends Controller
{
	protected $icon,
          $meta,
          $name,
          $option;

    public function __construct(){
        $this->middleware(['web', 'auth']);
        $this->option = new Option;
        $this->meta = $this->option->siteinfo('tagline');
        $this->icon = $this->option->siteinfo('icon');
        $this->name = $this->option->siteinfo('sitetitle');


    }

    public function doQuiz()
    {
        $credit = \App\Credit::all();
    	return view('do',[
    		'site'    =>  $this->site(),
            'credits' =>  $credit
    	]);
    }

    public function doQuizPostCredit(Request $request)
    {
        $credit = Credit::find($request->credit);
        if( $credit ){
            $questions = $credit->question()->where('valid', true)->get();
            if( $countQ = $questions->count() > 0 ){
                // $questions = $questions;
                session([ 'credit_id' => $credit->id, 'questions' => $questions, 'timer' => $this->_timer_($countQ) ]);
                return redirect()->route('doquiz');
            }
            else{
                $noques = "No Questions Avaible !";
                session(['noques'=> $noques]);
                return redirect()->route('doquiz');
            }
        }

    }

    public function statistics(StatisticRepository $repo)
    {
        return view('statistic',[
            'site'      =>  $this->site(),
            'statistic' =>  $repo->statistics(\Auth::user()->id),
            'repo'      =>  $repo
        ]);
    }

    public function certificate(StatisticRepository $repo, $id, $token)
    {
        $tok = session()->pull('tokenn');
        if( $tok != $token )
            return redirect(route('certificates'));

        return view('printCert',[
            'user'      =>  $repo->getCert($id)['name'],
            'grade'     =>  $repo->getCert($id)['marks'],
            'course'    =>  $repo->getCert($id)['credit'],
            'date'      =>  $repo->getCert($id)['date']
        ]);
    }

    public function certificates(StatisticRepository $repo)
    {
        session([ 'tokenn'   =>  str_random(40) ]);
        return view('certificates', [
            'site'          =>  $this->site(),
            'certificates'  =>  $repo->certificates(),
            'repo'          =>  $repo,
            'token'         =>  session()->get('tokenn')
        ]);
    }

    protected function _timer_($q = 1)
    {
        $t = 880;
        return $q == 1 ? $t : $t * $q;
    }


    public function postUserAnswers(Request $request)
    {
        session()->forget('credit_id');
        session()->forget('questions');

        $credit_id = $request->credit;
        $user_id = \Auth::user()->id;
        $course = Credit::find($credit_id);
        $course_id = $course->course_id;
        $order = Quiz::orderBy('order', 'desc')->where('user_id', $user_id)->first();
        $order = $order ? $order->order : 0;
        $order += 1;
        $questions = $request->questions;
        $countQ = count($questions);
        $trueAns = [];
        $t = 1;
        foreach ($questions as $question => $answer) {
            $findAns = Answer::find($answer);
            if( $findAns->iscorrect )
                $trueAns[] = $question;

            $_questions[$t]   =   $question;
            $_answers[$t]     =   $answer;
            $_collect[$t]     =   $findAns->iscorrect;
            $t += 1;
        }

        Quiz::create([
            'user_id'       =>  $user_id,
            'course_id'     =>  $course_id,
            'credit_id'     =>  $credit_id,
            'question_id'   =>  json_encode($_questions),
            'answered'      =>  json_encode($_answers),
            'collect'       =>  json_encode($_collect),
            'order'         =>  $order
        ]);

        $marks = [ count($trueAns), $countQ ];
        Student::create([
            'user_id'   =>  $user_id,
            'course_id' =>  $course_id,
            'credit_id' =>  $credit_id,
            'finished'  =>  true,
            'marks'     =>  json_encode($marks)
        ]);
        session(['msg' => "Please if your not above 80% you can't get Certificate"]);
        return redirect('statistics');
    }


	protected function site(){
        return [
            'icon'      =>  $this->icon,
            'meta'      =>  $this->meta,
            'name'      =>  $this->name,
            'logo'      =>  ! empty($this->icon) ? explode('icon.', $this->icon)[1] : null,
            'footer'    =>  [
                'social'    =>  $this->getSocialN(),
                'phones'    =>  $this->getNumer(),
                'logo'      =>  ''
            ]
        ];
    }

    protected function getSocialN(){
        $arr = [];
        if( $this->option->siteinfo('facebook') )
            $arr['facebook'] = $this->option->siteinfo('facebook');
        else $arr['facebook'] = null;
        if( $this->option->siteinfo('twitter') )
            $arr['twitter'] = $this->option->siteinfo('twitter');
        else $arr['twitter'] = null;
        if( $this->option->siteinfo('linkedin') )
            $arr['linkedin'] = $this->option->siteinfo('linkedin');
        else $arr['linkedin'] = null;
        if( $this->option->siteinfo('googleplus') )
            $arr['googleplus'] = $this->option->siteinfo('googleplus');
        else $arr['googleplus'] = null;

        return $arr;
    }

    protected function getNumer(){
        $arr = [];
        for($i = 1; $i <= 5; $i++){
            if( count($this->option->siteinfo('phone'.$i)) )
                $arr[] = $this->option->siteinfo('phone'.$i);
        }

        return $arr;
    }
}
