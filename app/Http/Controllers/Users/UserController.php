<?php

namespace App\Http\Controllers\Users;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\PageRepository;
use App\PageContent;
use App\Information;
use App\Option;
use App\Page;

class UserController extends Controller
{
    //
	protected $guard = "web",$user,$model;
	protected $icon,
              $meta,  
              $name,
              $option;

	public function __construct()
	{
		$this->middleware('auth');
		$this->user = Auth::user()->id;
		$this->user = User::find($this->user);
		$this->model = $this->user;
		if(Auth::check()){
			if(Auth::user()->active){
				$this->user->active = true;
				$this->user->save();
			}
		}

		$this->option = new Option;
        $this->meta = $this->option->siteinfo('tagline');
        $this->icon = $this->option->siteinfo('icon');
        $this->name = $this->option->siteinfo('sitetitle');

	}

	public function dashboard(UserRepository $user){

		if($user->hasPayed())
			return redirect('/');
		else 
			return redirect('/');
	}

	public function profile(UserRepository $user){
		return view('auth.layouts.user.profile',[
			'site'  =>  $this->site(),
			'user'	=>	$user
		]);
	}


	protected function site(){
        return [
            'icon'      =>  $this->icon,
            'meta'      =>  $this->meta,
            'name'      =>  $this->name,
            'logo'      =>  ! empty($this->icon) ? explode('icon.', $this->icon)[1] : null,
            'footer'    =>  [
                'social'    =>  $this->getSocialN(),
                'phones'    =>  $this->getNumer(),
                'logo'      =>  ''
            ]
        ];
    }

    protected function getSocialN(){
        $arr = [];
        if( $this->option->siteinfo('facebook') )
            $arr['facebook'] = $this->option->siteinfo('facebook');
        else $arr['facebook'] = null;
        if( $this->option->siteinfo('twitter') )
            $arr['twitter'] = $this->option->siteinfo('twitter');
        else $arr['twitter'] = null;
        if( $this->option->siteinfo('linkedin') )
            $arr['linkedin'] = $this->option->siteinfo('linkedin');
        else $arr['linkedin'] = null;
        if( $this->option->siteinfo('googleplus') )
            $arr['googleplus'] = $this->option->siteinfo('googleplus');
        else $arr['googleplus'] = null;
        
        return $arr;
    }

    protected function getNumer(){
        $arr = [];
        for($i = 1; $i <= 5; $i++){
            if( count($this->option->siteinfo('phone'.$i)) )
                $arr[] = $this->option->siteinfo('phone'.$i); 
        }

        return $arr;
    }

}
