<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\GalleryRepository;
use App\Page;
use App\Option;


class GalleryController extends Controller
{
    protected $icon,
              $meta,  
              $name,
              $option;

    public function __construct(){
        $this->middleware('web');
        $this->option = new Option;
        $this->meta = $this->option->siteinfo('tagline');
        $this->icon = $this->option->siteinfo('icon');
        $this->name = $this->option->siteinfo('sitetitle');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(GalleryRepository $gallery)
    {
        return view('gallery',[
            'page'  =>  $gallery,
            'site'  =>  $this->site()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $this->middleware('admin');
        if( $request->tk == \Session::get('del-gal') ){
            $page = \App\Page::where('slug','gallery')->firstOrFail();
            $content = [];
            foreach (json_decode($page->content,true) as $value) {
                if($value != $request->image ){
                    $content[] = $value;
                }
            }
            \File::delete(public_path().'/img/gallery/'.$request->image);
            $content = json_encode($content);
            $page->content = $content;
            $page->save();
            return $request->image;
        }
    }
    protected function site(){
        return [
            'icon'      =>  $this->icon,
            'meta'      =>  $this->meta,
            'name'      =>  $this->name,
            'logo'      =>  ! empty($this->icon) ? explode('icon.', $this->icon)[1] : null,
            'footer'    =>  [
                'social'    =>  $this->getSocialN(),
                'phones'    =>  $this->getNumer(),
                'logo'      =>  ''
            ]
        ];
    }

    protected function getSocialN(){
        $arr = [];
        if( $this->option->siteinfo('facebook') )
            $arr['facebook'] = $this->option->siteinfo('facebook');
        else $arr['facebook'] = null;
        if( $this->option->siteinfo('twitter') )
            $arr['twitter'] = $this->option->siteinfo('twitter');
        else $arr['twitter'] = null;
        if( $this->option->siteinfo('linkedin') )
            $arr['linkedin'] = $this->option->siteinfo('linkedin');
        else $arr['linkedin'] = null;
        if( $this->option->siteinfo('googleplus') )
            $arr['googleplus'] = $this->option->siteinfo('googleplus');
        else $arr['googleplus'] = null;
        
        return $arr;
    }

    protected function getNumer(){
        $arr = [];
        for($i = 1; $i <= 5; $i++){
            if( count($this->option->siteinfo('phone'.$i)) )
                $arr[] = $this->option->siteinfo('phone'.$i); 
        }

        return $arr;
    }
}
