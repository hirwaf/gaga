<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\IndexRepository;
use App\Repositories\PageRepository;
use App\Page;
use App\Option;
use App\PageContent;
use App\Repositories\SubscriberRepository;

class WebController extends Controller
{
    //
    protected $icon,
              $meta,  
              $name,
              $option;

    public function __construct(){
        $this->middleware('web');
        $this->option = new Option;
        $this->meta = $this->option->siteinfo('tagline');
        $this->icon = $this->option->siteinfo('icon');
        $this->name = $this->option->siteinfo('sitetitle');
    }
    
    public function index(IndexRepository $index){
        return view('welcome',[
            'site'  =>  $this->site(), 
            'index' =>  $index,
        ]);
    }
    
    public function registor(){
        
    }

    public function subscribe(Request $request, SubscriberRepository $sub)
    {
        $val = $this->validate($request, [
            'email' =>  'required|email'
        ]);
        if( $val == null )
            $sub->store($request);

        return redirect('');
    }

    protected function site(){
        return [
            'icon'      =>  $this->icon,
            'meta'      =>  $this->meta,
            'name'      =>  $this->name,
            'logo'      =>  ! empty( $this->icon ) ? explode('icon.', $this->icon)[1] : null,
            'footer'    =>  [
                'social'    =>  $this->getSocialN(),
                'phones'    =>  $this->getNumer(),
                'logo'      =>  ''
            ]
        ];
    }

    protected function getSocialN(){
        $arr = [];
        if( $this->option->siteinfo('facebook') )
            $arr['facebook'] = $this->option->siteinfo('facebook');
        else $arr['facebook'] = null;
        if( $this->option->siteinfo('twitter') )
            $arr['twitter'] = $this->option->siteinfo('twitter');
        else $arr['twitter'] = null;
        if( $this->option->siteinfo('linkedin') )
            $arr['linkedin'] = $this->option->siteinfo('linkedin');
        else $arr['linkedin'] = null;
        if( $this->option->siteinfo('googleplus') )
            $arr['googleplus'] = $this->option->siteinfo('googleplus');
        else $arr['googleplus'] = null;
        
        return $arr;
    }

    protected function getNumer(){
        $arr = [];
        for($i = 1; $i <= 5; $i++){
            if( count($this->option->siteinfo('phone'.$i)) )
                $arr[] = $this->option->siteinfo('phone'.$i); 
        }

        return $arr;
    }
    
    public function staticPage($slug){
        
        if($slug == 'admin')
            return redirect()->route('login.super');
        elseif( $slug == 'client' )
            return redirect()->route('login.user');
        elseif( $slug == 'login' )
            return redirect()->route('login.user');
        
        $page = Page::where('slug',$slug)->get()->first();
        if($page == null )
            return abort(404);
        $content = $page->content;
        if( $page->static )
            $content = !empty($page->pagecontent()->first())? $page->pagecontent()->first()->text : null;
        


        return view('blank',[
            'site'  =>  $this->site(),
            'page'  =>  new PageRepository($slug),
        ]);
    }
}
