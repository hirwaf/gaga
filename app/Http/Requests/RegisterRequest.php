<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterRequest extends Request
{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname'     =>  'required|min:3',
            'lastname'      =>  'required|min:3',
            'gender'        =>  'required',
            'district'      =>  'required',
            'sector'        =>  'required',
            'telephone'     =>  'required|min:12|unique:information,telephone',  
            'email'         =>  'required|email|unique:information,email',
            'password'      =>  'required|confirmed|min:8',
            'proffessional' =>  'required|min:2',
            'picture'       =>  'required',
            'cv'            =>  'required'
        ];
    }
}
