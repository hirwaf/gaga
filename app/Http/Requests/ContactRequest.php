<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContactRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      =>  'required|min:4|string',
            'email'     =>  'required|email',
            'message'   =>  'required|min:10'
        ];
    }

    public function messages()
    {
        return [
            'name.required'     =>  'Please, We need your name !',
            'name.string'       =>  'Check your name please',
            'email.required'    =>  'Please, Provide your email for feedback',
            'message.required'  =>  'You didn\'t say your message !!',
            'message.min'       =>  'Your message need to have meaning please '
        ];
    }
}
