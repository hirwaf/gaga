<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Repositories\UserRepository;

class HasPayedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$user = new UserRepository();
        if(!$user->hasPayed(Auth::user()->information_id) ){
		return redirect('nopayment');
        }

        return $next($request);
    }
}
