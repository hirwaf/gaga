<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    public function credit(){
        return $this->belongsTo('App\Credit');
    }
    
    public function answer(){
        return $this->hasMany('App\Answer');
    }
    
    public function response(){
        return $this->hasMany('App\Response');
    }
    
    
}
