<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    //
    public function course(){
        return $this->belongsTo('App\Course');
    }
    
    public function question(){
        return $this->hasMany('App\Question');
    }
    
    public function media(){
        return $this->belongsTo('App\Media');
    }

    public function student(){
        return $this->belongsTo('App\Student');
    }
    
}
