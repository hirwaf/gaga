<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    //
    protected $fillable = [
    	'user_id', 'course_id', 'credit_id', 'question_id', 'answered', 'collect', 'order'
   	];
}
