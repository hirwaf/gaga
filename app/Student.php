<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //
	protected $fillable = [
		'user_id', 'course_id', 'credit_id', 'finished', 'marks'
	];


    public function user(){
        $this->belongsTo('App\User');
    }

    public function credit(){
    	$this->hasMany('App\Credit');
    }
}
