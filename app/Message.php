<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //

    protected $fillable = [
    	'name','email','message', 'seen', 'read'
    ];

    protected $dates = [
    	'created_at','updated_at'
    ];
    
}
