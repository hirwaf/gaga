<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    //
    public function siteinfo($name=""){
    	if( $name != "" ){
            $op = $this->where('name', $name)->first();
    		return $op ? $op->value : "";
    	}
    	return "";
    }

}
