<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //
    public function credit(){
        return $this->hasMany('App\Credit');
    }
}
