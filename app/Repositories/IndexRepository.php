<?php

namespace App\Repositories;

use App\Page;
use App\Option;
use DB;

class IndexRepository
{
	private $slug = [
		'home',
		'index',
		'welcome'
	];

	public $title,
		   $content;

	public function __construct()
	{
		$option = new Option;
		$this->title = "Welcome to ".$option->siteinfo('sitetitle');
		$getpage = Page::where('slug',$this->slug[0])->first();
		$getpage = $getpage ? '' : Page::where('slug',$this->slug[1])->first();
		$getpage = $getpage ? '' : Page::where('slug',$this->slug[2])->first();
		$this->content =  $getpage->pagecontent()->first();
	}

	public function getSlider()
	{	if( $this->content )
			return json_decode($this->content->media);
		else 
			return null;	
	}

	public function getText()
	{
		if( $this->content )
			return $this->content->text;
		else
			return null;
	}

}