<?php

namespace App\Repositories;

use App\Page;
use App\Option;
use App\Course;


class CoursesRepository
{
	public $title,
		   $content;
	protected $option;	   
	public function __construct($slug='')
	{
		if($slug != '' ){
			$getpage = Page::where('slug',$slug)->first();
			$this->content = Course::orderBy('created_at','desc')->paginate(10); 
		}
		$this->option = new Option;
		$this->title = ucfirst($getpage->name) . " | ". $this->option->siteinfo('sitetitle');
	}

	public function newCourse($created = '')
	{

	}
	public function getCourse($course)
	{
		$show = Course::where('course',$course)->firstOrFail();
		$this->title = ucfirst($show->course) ." | " . $this->option->siteinfo('sitetitle');
		return $show;	
	}

	public function getFile($id)
	{
		$media = \App\Media::find($id);
		$name = $media->url.".".$media->type;

		return "files/".$name;
	}

}