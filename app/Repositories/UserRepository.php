<?php

namespace App\Repositories;

use App\Information;
use App\Student;
use App\Sector;
use App\Credit;
use App\Option;
use App\User;
use Auth;

class UserRepository
{
	public $information = null;
	private $option;

	public function __construct($id = null)
	{
		if (  Auth::check() ){
			$id = Auth::user()->information_id;
		}

		$this->information = Information::where('id',$id)->first();
		$this->option = new Option();
		#$this->title = $this->title->getSitetitle();	
	}

	public function getitle(){
		return $this->option->siteinfo('sitetitle');
	}

	public function getProfile(){
		$json  = $this->get()->images;
		$profile = json_decode($json);
		return asset("/users/images/".$profile->avatar);
	}

	public function hasPayed($id = null){
		$allowed = $this->get($id)->approved;
		return $allowed ? true : false;
	}

	public function getAddress($v = ""){
		if($v == "")
			$v = $this->get()->address;
		$address = Sector::find($v);
		$dis = $address->district->district_name;
		$pro = $address->district->province->province_name;
		return $pro.", ".$dis.", ".$address->sector_name;
	}

	
	public function getCv($op = null){
		$json = $this->get()->images;
		$cv = json_decode($json, true);
		$cv = $cv['cv'];
		// $foo = asset("/users/cv/".$cv);
		return $op != null ? $foo : $cv;
	}

	public function getOnCredit($id = null){
		if($id == null)
			$id = Auth::user()->id;
		$student = Student::where('user_id',$id)
							->where('finished',false)->first();
		$credit = Credit::find($student->credit_id);					

		return $credit;
	}

	public function getOffCredits($id = null){
		if($id == null)
			$id = Auth::user()->id;
		$student = Student::where('user_id',$id)
							->where('finished',true);
		$credits = [];
		foreach ($students as $student) {
			$credits[] =  Credit::find($student->credit_id);
		}				

		return $credits;
	}

	public function get($id = null){
		if($id == null){
			return $this->information;
		}
		else{
			return Information::where('id',$id)->first();		
		}
	}

	public function reverseApproval($id = null){
		$user = Information::findOrFail($id);
		if($user->approved == 1)
			$user->approved = 0;
		else
			$user->approved = 1;
		
		$user->save();
		
	}

	public static function getById($id = null){
		if($id == null){
			return $this->information;
		}
		else{
			return Information::where('id',$id)->first();		
		}	
	}

	public function getProffessional($id = null){
		$json = $this->get($id)->information;
		$prof = json_decode($json, true);
		$prof = $prof['prof'];

		return $prof;
	}

	public function getCVPath($cv){
		if( ! empty($cv) ){
			$filename = $cv;
			$exp = explode('.',$filename);
			$end = "";
			if(count($exp) > 0)
				$end = end($exp) != 'pdf' ? "" : "";

			$path = asset("users/cv".DIRECTORY_SEPARATOR.$cv.$end);
			// $path = public_path('users/cv/'.$filename.".pdf");
			// return asset("/users/cv/".$cv.'.pdf');
			// $response = \Response::make($path, 200, [
			// 	'Content-Type' => 'application/pdf',
			// 	'Content-Disposition' => 'inline;'.$filename,
			// ]);
			return $path;
		}
		return "";
	}
	
}