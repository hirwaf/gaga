<?php
namespace App\Repositories;
use App\Subscribe;

/**
* 
*/

class SubscriberRepository
{
	
	function __construct()
	{
		# code...
	}

	public function store($request)
	{
		$email = $request->email;
		$exist = Subscribe::where('email',$email)->count();
		if( $exist > 0 )
			return true;
		else{
			Subscribe::create([
				'email'	=>	$email
			]);
			return true;
		}
	}

	public function getAll()
	{
		return Subscribe::orderBy('created_at', 'desc')->paginate(20);
	}

	public function subCount()
	{
		return Subscribe::all()->count();
	}

	public function destroy($id)
	{
		Subscribe::find($id)->delete();
		return true;
	}
}