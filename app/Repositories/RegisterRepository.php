<?php

namespace App\Repositories;

use App\Information;
use App\Option;
use App\Page;
use App\User;
use Image;
use DB;

class RegisterRepository
{
	public $title,
		   $content;

	public function __construct($slug='registration')
	{
		if($slug != '' ){
			$getpage = Page::where('slug',$slug)->first();
			$this->content = $getpage->content; 
		}
		$option = new Option;
		$this->title = ucfirst($getpage->name) . " | ". $option->siteinfo('sitetitle');
	}

	public function toStore($request)
    {
        $c = Information::count() + 1;
        $avatarName = strtoupper($request->firstname{0}).time().$c.'.'.$request->file('picture')[0]->getClientOriginalExtension();
        $avatarPath = public_path().'/users/images/';
        $cvpath = public_path().'/users/cv/';
        $cvName = $request->firstname."_".$request->lastname;
        
        $avatar = Image::make($request->file('picture')[0]);
        $avatar->resize(200,200);
        $avatar->save($avatarPath.$avatarName);
        $cv = [];

        $o = 1;
        foreach ($request->file('cv') as $value) {
           $filename = $cvName.'_'.$c.'_'.$o.time().".pdf";
           $value->move($cvpath, $filename);
           $cv[] = $filename;
           $c++;
        }

        $images_ = [
            'avatar'    =>  $avatarName,
            'cv'        =>  $cv
        ];
        $images_ = json_encode($images_);
        $information = Information::create([
            'firstname'     =>  $request->firstname,
            'lastname'      =>  $request->lastname,
            'sex'           =>  $request->gender,
            'telephone'     =>  $request->telephone,  
            'email'         =>  $request->email,
            'images'        =>  $images_,
            'address'       =>  $request->sector,
            'information'   =>  json_encode(['prof' => $request->proffessional]),
            'approved'      =>  false
        ]);

        $user = new User;
        $user->information_id = $information->id;
        $user->email = $request->email;
        $user->telephone = $request->telephone;
        $user->password = bcrypt($request->password);
        $user->active = 0;
        $user->save();
    }

    public function isValidated($request)
    {
    	$val = \Validator::make($request->all(),$this->rules());
    	return $val;
    }

    public function rules()
    {
        return [
            'firstname'     =>  'required|min:3',
            'lastname'      =>  'required|min:3',
            'gender'        =>  'required',
            'district'      =>  'required',
            'sector'        =>  'required',
            'telephone'     =>  'required|min:10|unique:information,telephone',  
            'email'         =>  'required|email|unique:information,email',
            'password'      =>  'required|confirmed|min:8',
            'proffessional' =>  'required|min:2',
            'picture'       =>  'required',
            'cv'            =>  'required'
        ];
    }
}