<?php

namespace App\Repositories;

use App\Page;
use App\Option;
use DB;

class GalleryRepository
{
	public $title,
		   $content,
		   $perpage = 24,
		   $pages,
		   $all;

	public function __construct($slug='gallery')
	{
		if($slug != '' ){
			$getpage = Page::where('slug',$slug)->first();
			$this->content = is_array(json_decode($getpage->content,true))  ? json_decode($getpage->content,true) : []  ;
			$this->content = array_reverse($this->content)? array_reverse($this->content) : $this->content ;
			$this->all = count( json_decode($getpage->content,true) );
			$this->pages = ceil($this->all / $this->perpage);
		}
		$option = new Option;
		$this->title = ucfirst($getpage->name) . " | ". $option->siteinfo('sitetitle');
	}
}