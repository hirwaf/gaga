<?php

namespace App\Repositories;

use App\Page;
use App\Option;
use DB;

class PageRepository
{
	public $title,
		   $content;

	public function __construct($slug='')
	{
		if($slug != '' ){
			$getpage = Page::where('slug',$slug)->first();
			$this->content = $getpage->content; 
		}
		$option = new Option;
		$this->title = ucfirst($getpage->name) . " | ". $option->siteinfo('sitetitle');
	}
}