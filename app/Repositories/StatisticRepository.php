<?php

namespace App\Repositories;
use App\Credit;
use App\Course;
use App\Student;
use App\Answer;
use App\Quiz;
use App\Question;
use App\User;
use App\Information;
/**
*
*/
class StatisticRepository
{

	function __construct()
	{
		# code...
	}

	public function statistics($userId)
	{
		$quizz = Quiz::orderBy('order', 'desc')
					  ->where('user_id', $userId)
					  ->get();
		return $quizz;
	}

	public function getCourse($id)
	{
		return strtoupper(Course::find($id)->course);
	}

	public function getCredit($id)
	{
		return ucfirst(Credit::find($id)->title);
	}

	public function getQuestion($id)
	{
		return ucfirst(Question::find($id)->question);
	}

	public function getAnswer($id)
	{
		return ucfirst(Answer::find($id)->answer);
	}

	public function json($json)
	{
		return json_decode($json, true);
	}

	public function marks($id)
	{
		$get = Quiz::find($id);
		$co = json_decode($get->collect,true);
		$co = $co ? $co : [];
		$c = count($co);

		$p = 0;
		foreach ($co as $key => $value) {
			if( $value == 1 )
				$p++;
		}
		if( $c != 0 )
			$pp = ceil((100*$p)/$c);
		else
				$pp = 0;
				
		$k = $pp < 80 ? $pp < 50? "label-danger" : "label-warning" : "label-primary";
		return [$pp."%",$k];
	}

	public function getCert($id)
	{
		$std = Student::findOrFail($id);
		$user = User::findOrFail($std->user_id);
		$course = Credit::findOrFail($std->credit_id)->title;
		$date = $std->created_at;

		$info = Information::findOrFail($user->information_id);
		$name = $info->firstname." ".$info->lastname;

		$m = json_decode($std->marks, true);
		$m = $m ? $m : [0,1];
		$cm = ceil(( 100 * $m[0] )/ $m[1]);
		$marks = $cm;

		if( $marks < 80 )
			return abort(404);

		return [
			'name'		=>	$name,
			'credit'	=>	$course,
			'date'		=>	$date,
			'marks'		=>	$marks
		];

	}

	public function certificates()
	{
		$certs = Student::orderBy('created_at', 'desc')
						->where('user_id', \Auth::user()->id)
						->where('finished', true)->get();
		$real = [];
		foreach ($certs as $cert) {
			if( $this->isAbove( $cert->marks )[0] ){
				$real[] = $cert;
			}
		}

		return $real;
	}

	public function isAbove($m)
	{
		$m = $this->json($m);
		$m = $m ? $m : [0,1];
		$cm = ceil(( 100 * $m[0] )/ $m[1]);
		$k = $cm < 80 ? false : true;

		return [$k,$cm];
	}

	public function countA($arr)
	{
		return count($arr);
	}

}
