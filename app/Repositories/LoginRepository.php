<?php

namespace App\Repositories;

use App\Page;
use App\Option;

class LoginRepository
{
	public $title,
		   $content;

	public function __construct($slug='login')
	{
		if($slug != '' ){
			$getpage = Page::where('slug',$slug)->first();
			$this->content = $getpage->content; 
		}
		$option = new Option;
		$this->title = ucfirst($getpage->name) . " | ". $option->siteinfo('sitetitle');
	}
}