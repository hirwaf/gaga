<?php
namespace App\Repositories;
use App\Message;
use App\Http\Requests\ContactRequest;
/**
* 
*/
class ContactRepository
{
	protected 	$request;
	public		$pass_,
				$errors_;

	function __construct()
	{
		$this->request = new  ContactRequest;
		$this->pass_ = false;
		$this->errors_ = [];
	}

	public function store($request)
	{	
		$this->validates($request);
		
		if( $this->pass_ ){

			Message::create([
				'name' 		=> 	$request->name,
				'email'		=>	$request->email,
				'message'	=>	$request->message
			]);
		}
		else{
			$this->pass_ = false;
			$this->errors_[] = "Sending error try again !";
		}
		
		return $this->pass_;
	}
	public function getMessages()
	{
		$msg = Message::orderBy('created_at', 'desc')
						->where('seen', 0)
						->where('read', 0)
						->get();
		return $msg;
	}

	public function newMsg()
	{
		$count = Message::where('seen', 0)
						->where('read', 0)
						->count();
		return $count;				
	}

	public function header()
	{
		$d = $this->newMsg();
		return $this->newMsg() > 0 ? $this->newMsg() == 1 ? "You have a message" : "You have $d messages" : "";
	}

	protected function validates($request){
		$validator = \Validator::make($request->all(), $this->request->rules());
		if( ! $validator->fails() )
			$this->pass_ = true;
		else{
			$this->pass_ = false;
			$this->errors_ = $validator->errors()->all();
		}

	}

}