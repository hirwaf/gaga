@extends('layouts.app')

@section('title','Statistics')

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2"> 
		@if( $repo->countA( $certificates ) > 0 )
			<table class="table ">
			<thead>
				<th>#</th>
				<th>Course Name</th>
				<th>Marks</th>
				<th>Date</th>
				<th></th>
			</thead>
			<tbody>
			<?php $r = 1; ?>
			@foreach( $certificates as $cert )
				<tr>
					<td>{{ $r }}</td>
					<td><label class="label label-primary" style="font-size: 14px;">{{ ucfirst($repo->getCredit( $cert->credit_id )) }}</label></td>
					<td><label class="label label-primary" style="font-size: 14px;">{{ $repo->isAbove( $cert->marks )[1] }}%</label></td>
					<td><label class="label label-primary" style="font-size: 14px;">{{ $cert->created_at->diffForHumans() }}</label></td>
					<td>
						<a href="{{ url(route('viewcert',[$cert->id, $token])) }}" class="btn btn-block btn-sm btn-primary">
							View
						</a>
					</td>
				</tr>
			<?php $r++ ?>	
			@endforeach
			</tbody>
			</table>
		@else
		<h1 class="text-center">You don't have any Certificate</h1>
		@endif
		</div>
	</div>
</div>	
<div class="clear-fix">&nbsp;</div>
@endsection

@section('styles')

@endsection

@section('scripts')
<script type="text/javascript">
	$(function(){
		$('form ').addClass(' form ');
		$('* input ').addClass(' form-control ');
		$('* textarea ').addClass(' form-control ');
	});
</script>
@endsection
