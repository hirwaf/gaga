<div id="myc" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 carousel" >
@if( count($index->getSlider()) > 0 )
    <ol  class="carousel-indicators">
        @foreach($index->getSlider() as $key => $slide)
            <li data-target="#myc" data-slide-to="{{ $key }}" class="<?= ( $key == '0' )? 'active' : '' ?>"></li>
        @endforeach
    </ol>
    <div class="carousel-inner" >
        @foreach($index->getSlider() as $key => $slide)
            <div class="item <?= ( $key == '0' )? 'active' : '' ?>">
                <img src="/img/slides/{{ $slide->image }}" alt="{{ $slide->title }}" class="img-responsive">
                <div class="carousel-caption">
                    <h3>{{ $slide->title }}</h3>
                    <p>{{ $slide->desc }}</p>
                </div>
            </div>
        @endforeach
        <!--  -->
    </div>
    <!-- Controls -->
      <a class="left carousel-control" href="#myc" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myc" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
@endif      
</div>
<style type="text/css">
    #myc img{
        height: 450px;
    }
    .left{
        margin-left: 15px; 
    }
    .right{
        margin-right: 15px; 
    }
    @media only screen and (max-width: 768px){
        #myc img{
            height: 200px;
        }   
    }
</style>