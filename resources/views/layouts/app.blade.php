<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="{{ $site['meta'] }}"/>
    <link rel="icon" href="{{ asset(url('img/icon/'.$site['icon'])) }}">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ asset(url('css/bootstrap.min.css')) }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset(url('css/style.css')) }}">
    <link href='https://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'>
    <script src="{{ asset('plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
    @yield('styles')
    @yield('scripts_t')
</head>
<body id="app-layout">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 head">
                <h1>{{ strtoupper($site['name']) }}</h1>
            </div>
            @yield('menu')
        </div>
        @yield('content')
    </div>
    <footer class="nav navbar-inverse  col-xs-12 col-sm-12" role="navigation">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 page-header footer">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <h3 class="text-center page-header footer">Our social networks</h3>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 icon-holder">
                            @if( $site['footer']['social']['twitter'] )
                            <a href="{{ parse_url($site['footer']['social']['twitter'], PHP_URL_SCHEME)? '' : 'https://'.$site['footer']['social']['twitter'] }}" target="__blank" class="btn btn btn-social-icon btn-twitter"><i class="fa fa-twitter icon"></i></a>
                            @endif
                            @if( $site['footer']['social']['facebook'] )
                            <a href="{{ parse_url($site['footer']['social']['facebook'], PHP_URL_SCHEME)? '' : 'https://'.$site['footer']['social']['facebook'] }}" target="__blank" class="btn btn btn-social-icon btn-facebook"><i class="fa fa-facebook icon"></i></a>
                            @endif
                            @if( $site['footer']['social']['linkedin'] )
                            <a href="{{ parse_url($site['footer']['social']['linkedin'], PHP_URL_SCHEME)? '' : 'https://'.$site['footer']['social']['linkedin'] }}" target="__blank" class="btn btn btn-social-icon btn-linkedin"><i class="fa fa-linkedin icon"></i></a>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <h3 class="text-center page-header footer">Direct Help, please call to</h3>
                         @foreach($site['footer']['phones'] as $phone )
                            <h4 class="text-center"> {{ "(+".substr($phone,0,3).") ".substr($phone,3) }} </h4>
                         @endforeach
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 pull-right">
                        <div class="clearfix">&nbsp;</div>
                        <div class="clearfix">&nbsp;</div>
                        <div class="logo-img">
                            <img src="{{ asset(url('img/icon/'.$site['logo'])) }}" class="img img-resiponsive" >
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
    </footer>
    
    <!-- InputMask -->
    <script src="{{ asset(url('plugins/input-mask/jquery.inputmask.js')) }}"></script>
    <script src="{{ asset(url('plugins/input-mask/jquery.inputmask.date.extensions.js'))  }}"></script>
    <script src="{{ asset(url('plugins/input-mask/jquery.inputmask.extensions.js')) }}"></script>
    <!-- Select2 -->
    <script src="{{ asset(url('plugins/select2/select2.full.min.js')) }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
    <!-- Web js -->
    <script src="{{ asset('js/web.js') }}"></script>
    @yield('scripts')
</body>
</html>
