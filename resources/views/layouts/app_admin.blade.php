<?php
  $options = new \App\Option;
  $icon = $options->siteinfo('icon');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="{{ asset(url('img/icon/'.$icon)) }}" >
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    @yield('style_p')
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('css/skins/_all-skins.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.a.css') }}">
    <script src="{{ asset('plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset(url('css/short.css')) }}">
    @yield('styles')
    @yield('scripts_top')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">

    <!-- Logo -->
    <a href="{{ url(route('dashboard.super')) }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>EP</b>rw</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>E-Physio</b>rw</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            @if( $contact->newMsg() > 0 )
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">{{ $contact->newMsg() }}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">{{ $contact->header() }}</li>
                <li>
                  <ul class="menu">
                    @foreach ($contact->getMessages() as $message)
                        <!-- inner menu: contains the actual data -->
                          <li><!-- start message -->
                            <a  style="cursor: pointer;" 
                                message="{{ $message->message }}" 
                                name="{{ $message->name }}"
                                email="{{ $message->email }}"
                                class="message"
                                messaged = "{{ $message->id }}"
                                >
                              <h4>
                                {{ $message->name }}
                                <small><i class="fa fa-clock-o"></i>{{ $message->created_at->diffForHumans() }}</small>
                              </h4>
                              <p>{{ substr($message->message, 0,35)." ..." }}</p>
                            </a>
                          </li>
                    @endforeach
                  </ul>
                </li>
              <li>&nbsp;</li>
              <li>
              <li> <a class="markasread" style="cursor: pointer;">Mark as Read</a></li>
              </li>
            </ul>
            @endif
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <div class="fa fa-gear"></div>
              <span class="hidden-xs">
                <?= Auth::guard('admins')->user()->name; ?>
              </span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <center><div class="avatar avatar-lg"><?= Auth::guard('admins')->user()->name{0}; ?></div></center> 
                <p>
                  <?= Auth::guard('admins')->user()->name; ?>
                  <small>I'm the administrator</small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{ url(route('profile.super')) }}" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="{{ url(route('logout.super')) }}" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->

  <aside class="main-sidebar" style="font-size: small !important;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <div class="avatar avatar-md"><?= Auth::guard('admins')->user()->name{0}; ?></div>
        </div>
        <div class="pull-left info">
          <p> <?= Auth::guard('admins')->user()->name; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="{{ Route::current()->getName() == 'dashboard.super' ? 'active' : '' }} treeview">
          <a href="{{ url(route('dashboard.super')) }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        @yield('menu')  
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  @yield('content')
  <!-- /.content-wrapper -->

<div class="modal fade view-msg" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p class="col-sm-12" style="text-align: justify;"></p>
          <hr>
          <form action="{{ url(route('send.email.super')) }}" method="post">
            {!! csrf_field() !!}
            <input type="hidden" name="names" id="name" value="">
            <div class="form-group">
              <label class="control-label col-sm-2" for="to">To</label>
              <div class="col-sm-10">
                <input type="text" name="email" id="email" value="" readonly="" class="form-control">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="message">Repy Message</label>
              <div class="col-sm-10">
                <textarea autofocus name="message" id="message" class="form-control" rows="5" required></textarea>
              </div>
            </div>
        </div>
          <div class="clearfix">&nbsp;</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Reply</button>
        </div>
          </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
       
    </div>
    <strong>Copyright &copy; 2014-2015 <a href="http://ndagano.com">Hirwa felix</a>.</strong> All rights
    reserved.
  </footer>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->
    
    <!-- Select2 -->
    @yield('script_p')
    <script src="{{ asset(url('plugins/select2/select2.full.min.js')) }}"></script>
    <!-- InputMask -->
    <script src="{{ asset(url('plugins/input-mask/jquery.inputmask.js')) }}"></script>
    <script src="{{ asset(url('plugins/input-mask/jquery.inputmask.date.extensions.js'))  }}"></script>
    <script src="{{ asset(url('plugins/input-mask/jquery.inputmask.extensions.js')) }}"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="{{ asset(url('plugins/daterangepicker/daterangepicker.js')) }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset(url('plugins/datepicker/bootstrap-datepicker.js')) }}"></script>
    <!-- bootstrap color picker -->
    <script src="{{ asset(url('plugins/colorpicker/bootstrap-colorpicker.min.js')) }}"></script>
    <!-- bootstrap time picker -->
    <script src="{{ asset(url('plugins/timepicker/bootstrap-timepicker.min.js')) }}"></script>
    <!-- SlimScroll -->
    <script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
    <script src="{{ asset(url('plugins/iCheck/icheck.min.js')) }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ asset('js/demo.js') }}"></script>
    <script src="{{ asset(url('js/__init__.js')) }}"></script>
    <script type="text/javascript">
      $(function(){
        $(".message").on('click', function(e){
          var name = $(this).attr('name');
          var email = $(this).attr('email');
          var message = $(this).attr('message');
          var url_ = $(this).attr('mark-as-read');
          var id =  $(this).attr('messaged');
          var modal = $(".view-msg");
          modal.find('.modal-title').text(name);
          modal.find('#name').attr('value', name);
          modal.find('.modal-body p').html(message);
          modal.find('#email').attr('value', email);
          $.post(
            '/admin/markasread',
            {
              id : id,
              _token : "{{ csrf_token() }}"
            },
            function(data) {
            }
          );
          modal.modal();
        });
        $(".markasread").on('click', function(e){
          $.post(
            '/admin/markasread',
            {
              id : 'a',
              _token : "{{ csrf_token() }}"
            },
            function(data) {
              window.location = "{{ url(route(Route::current()->getName(),session()->pull('_id_'))) }}"
            }
          );
        });
      });
    </script>
    @if( session()->has('flash_') )
    <?php $fl = session()->pull('flash_'); ?>
    <script type="text/javascript">
      alert("{{ $fl }}");
    </script>
    @endif
    @yield('script')

</body>
</html>
