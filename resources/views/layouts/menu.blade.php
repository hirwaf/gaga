<?php
    $menus = \App\Menu::orderBy('order','asc')
                        ->where('active',true)
                        ->where('only',false)
                        ->get();
    $auth_ = false;
    $g = 1;
    if ( auth()->check() ) {
        $menus = \App\Menu::orderBy('order', 'asc')
                            ->where('active', true)
                            ->where('auth', true)
                            ->orWhere('only',true)
                            ->get();
        $auth_ = true;
        $user = new \App\Repositories\UserRepository;
    }
?>
<!-- Menus -->
<br>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-8 hidden-sm hidden-xs hidden-md pull-right">
    <nav class="navbar navbar-default navbar-static-top" role="navigation ">
        <div class="">  
            <ul class="nav navbar-nav">
                 @foreach($menus as $menu)
                    <?php
                        $pagenation = ($menu->page->slug == 'gallery' )? '?p=1' : ''
                    ?>
                    @if( $auth_ && $g == 2 && $menu->page->only )
                        <li><a href="{{ url($menu->page->slug).$pagenation }}">{{$menu->page->name}}</a><li>
                    @endif
                    <li>
                        <a href="{{ url($menu->page->slug).$pagenation }}">{{$menu->page->name}}</a>
                    </li>
                    <?php $g++; ?>
                 @endforeach
                 @if($auth_)
                 <li class="dropd">
                    <a class="dropdme" href="cpd.php">{{ ucfirst($user->get()->firstname) }}</a>
                    <ul class="dropdcont pull-right">
                        <li><a href="{{ url(route('profile.user')) }}">Profile</a>
                        <li><a href="{{ url(route('logout.user')) }}">Logout</a>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
    </nav>
</div>

<div class="row">
    <div class="col-md-12 col-lg-3  hidden-lg">
        <div class="list-group">
            @foreach($menus as $menu)
                <a href="{{ url($menu->page->slug) }}" class="list-group-item"><h4 class="list-group-item-heading">{{$menu->page->name}}</h4></a>
            @endforeach 
        </div>
    </div>
</div>
