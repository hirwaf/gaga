@extends('layouts.app')

@section('title',$index->title)

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
<!-- Slider -->
<div class="row">
    @include('layouts.slide') 
</div>
<!-- Body -->
<div class="row" style="text-align: justify;">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
	<?php print($index->getText()) ?>	
    </div> 
    
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-3">
        <div class="caption">
            <h1>Contact us</h1><br>
            @if( count($site['footer']['phones']) )
            <h4>{{ "(+".substr($site['footer']['phones'][0],0,3).") ".substr($site['footer']['phones'][0],3) }}</h4>
            @endif
        </div>
        <div class="caption page-header">
            <a href="contact-us">Send Us an Email</a>
        </div>
        <div class="caption">
            <button type="button" class="btn btn-primary btn-sub">Subscribe</button>
            <p><br>
                Subcribe Us to get the latest news and send us an email to ask for more info
            </p>
        </div>
    </div>
</div>
<div class="modal fade sub-modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Subscribe</h4>
      </div>
      <div class="modal-body">
        <form action="{{ url('/welcome') }}" method="post">
        {{ csrf_field() }}
  		<div class="form-group">
    			<label for="exampleInputEmail1">Email address</label>
    			<input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
  		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Subscribe</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('styles')
@endsection

@section('scripts')
<script type="text/javascript">
    $(function(){
        $('.carousel').carousel({
          wrap: true,
          interval: 3000
        });
        $('.btn-sub').on('click', function(){
        	$('.sub-modal').modal();
        });
    });
</script>
@endsection