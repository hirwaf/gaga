@extends('layouts.app')

@section('title',$page->title)

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
<div class="row">
	<?php
		print($page->content);
	?>
</div>
<div class="clear-fix">&nbsp;</div>
@endsection

@section('styles')

@endsection

@section('scripts')
<script type="text/javascript">
	$(function(){
		$('form ').addClass(' form ');
		$('* input ').addClass(' form-control ');
		$('* textarea ').addClass(' form-control ');
	});
</script>
@endsection