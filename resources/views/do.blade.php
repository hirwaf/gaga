@extends('layouts.app')

@section('title',"Make Quiz")

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		@if( ! session()->has('credit_id') )
		<form role="form" action="{{ url(route('next.to.question')) }}" method="post" >
		{{ csrf_field() }}
		    <div class="row">
		        <div class="col-xs-12">
		            <div class="col-md-12">
		                <h3> Choose a Credit</h3>
		                @if( $credits->count() > 0 )
		                <div class="form-group">
		                    <label class="control-label col-sm-12">Credit Name</label>
		                    <div class="col-sm-5">
		                    	<select required class="form-control select2" name="credit" >
			                    	<option selected="selected" value="" disabled >Select Credit</option>
			                    	@foreach( $credits as $credit )
			                    	<option value="{{ $credit->id }}">{{ ucfirst($credit->title) }}</option>
			                    	@endforeach
			                    </select>
		                    </div>
		                </div>
		                <div class="clearfix">&nbsp;</div>
		                <div class="clearfix">&nbsp;</div>
		                <div class="form-group">
		                	<div class="col-sm-5">
		                		<button class="btn btn-primary nextBtn btn-md pull-right" type="submit" >Next</button>
		                	</div>
		                </div>
		                @else
		                <div class="form-group">
		                	<h3>There is no credit avaible!!</h3>
		                </div>
		                @endif 
		            </div>
		        </div>
		    </div>
		</form>
		@endif
		@if( session()->has('credit_id') && session()->has('questions') )
		<?php
			$questions = session()->pull('questions');
			$time = session()->pull('timer');
		?>
		<form role="form" action="{{ url(route('submit.answers')) }}" method="post" id="quiz" >
		{{ csrf_field() }}
		<input type="hidden" name="credit" value="{{ session()->pull('credit_id') }}" >
		    <div class="row">
		        <div class="col-xs-12">
		            <div class="col-md-12">
		                <h3> Questions</h3>
		                <div class="clearfix">&nbsp;</div>
		                @if( $questions->count() > 0 )
		                <div class="col-sm-5">
			                <div id="progress"><div id="bar"></div></div>
		                </div>
		                <div class="clearfix">&nbsp;</div>
		                @foreach( $questions as $key => $question )
                    	@if( $question->answer()->count() > 0 )
		                <div class="form-group">
		                    <label class="control-label col-sm-12"> <span class="label label-primary">{{ ++$key }}</span>&nbsp;&nbsp;{{ ucfirst($question->question) }}</label>
		                    <div class="col-sm-5">
			                    <select name="questions[{{ $question->id }}]" class="form-control" >
			                    	<?php $answers = $question->answer()->get() ?>
			                    	<option value="null" selected disabled>Select Your Answer</option>
			                    	@foreach( $answers as $answer )
			                    	<option value="{{ $answer->id }}">{{ $answer->answer }}</option>
			                    	@endforeach	
			                    </select>
				                <hr>
		                    </div>
		                </div>
                    	@endif
		                @endforeach
		                <div class="clearfix">&nbsp;</div>
		                <div class="clearfix">&nbsp;</div>
		                <div class="form-group">
		                	<div class="col-sm-4">
				                <div id="progress"><div id="bar2"></div></div>
			                </div>
		                	<div class="col-sm-5">
		                		<button class="btn btn-success btn-md pull-lef" type="submit" id="finish">Finish!</button>
		                	</div>
		                </div>
		                <div class="clearfix">&nbsp;</div>
		                @else
		                <div class="form-group">
		                	<h3>There is no credit avaible!!</h3>
		                </div>
		                @endif
		            </div>
		        </div>
		    </div>
		</form>
		<script>
			move();
			function move() {
			  var elem = document.getElementById("bar");
			  var elem2 = document.getElementById("bar2");
			  var width = 100;
			  var tm = parseInt("{{ $time }}");
			  var id = setInterval(frame, tm);
			  function frame() {
			    if (width == 0) {
			      clearInterval(id);
			      var r = document.getElementById("quiz");
			      r.submit();
			    } else {
			      width--;
			      elem.style.width = width + '%';
			      elem2.style.width = width + '%';
			      if(width < 30 ){
				      	elem.style.backgroundColor = "#f00";
				      	elem2.style.backgroundColor = "#f00";
			      }
			    }
			  }
			}
		</script>
		<style>
			#progress {
			  width: 100%;
			  height: 10px;
			  position: relative;
			  background-color: #ddd;
			  margin-bottom: 5px;
			  border-radius: 7em;
			}

			#bar,#bar2 {
			  background-color: #4CAF50;
			  width: 100%;
			  height: 10px;
			  text-align: right;
			  position: absolute;
			  border-radius: 7em;
			}
		</style>

		@endif
	</div>
</div>
<div class="clear-fix">&nbsp;</div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset(url('plugins/select2/select2.min.css')) }}">
<link rel="stylesheet" type="text/css" href="{{ asset(url('css/quiz.css')) }}">
@endsection

@section('scripts')
<script src="{{ asset(url('plugins/select2/select2.full.min.js')) }}"></script>
<script type="text/javascript" src="{{ asset(url('js/quiz.js')) }}"></script>
<script type="text/javascript">
	$(function(){
	    $(".select2").select2();
	});
</script>
@if( session()->has('noques') )
<script type="text/javascript">
	$(function(){
		alert("{{ session()->pull('noques')}}");
	});
</script>
@endif
@endsection
