@extends('layouts.app')

@section('title',$page->title)

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h1>Here you login to start </h1>
	</div>
	</div>
	<div class="row">
	  <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 ">
			@if (session()->has('feedback'))
				<div class="alert alert-danger alert-dismissible">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		            {{ session()->pull('feedback') }}
				</div>
	        @endif
			<table class="table col-xs-12 col-sm-12 col-md-8 col-lg-8 log" >
				<form role='form' method="post" action="{{ url(route('login.user')) }}" >
				{{ csrf_field() }}
					<tbody class=" " >
						<tr>
							<td>Email or Telephone</td>
							<td><input type="text" name="email" autocomplete="off" autofocus ></td>
						</tr>
						<tr>
							<td>Password</td>
							<td><input type="password" name="password"></td>
						</tr>
						<tr>
							<td><a class="btn btn-link" href="/client/reset-password" >Forget password?</a></td>
							<td><button type="submit" class="btn btn-default register ">Login</button></td>
							<td>
								<a href="/registration" class="btn btn-default" > Create Account </a>
							</td>
						</tr>
						
					</tbody>
				</form>
			</table>
	  </div>
	</div>
</div>
<div class="clear-fix">&nbsp;</div>
@endsection

@section('styles')

@endsection

@section('scripts')
<script type="text/javascript">
	$(function(){
		$('form ').addClass(' form ');
		$('* input ').addClass(' form-control ');
		$('* textarea ').addClass(' form-control ');
	});
</script>
@endsection