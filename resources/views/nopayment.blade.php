@extends('layouts.app')

@section('title',"No Payment")

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
<div class="row">
<h1>Please You Have Not Payed </h1>
<br>
<span class='text-info text-lg'>Click <a href='{{ url("payments") }}'>Here to see payment methods</a> </span>

</div>
<div class="clear-fix">&nbsp;</div>
@endsection

@section('styles')

@endsection

@section('scripts')

@endsection