@extends('layouts.app')

@section('title',$page->title)

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h1>{{ ucfirst($course->course) }}</h1>
		<p>
			{{ ucfirst($course->summary) }}
		</p>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12">
	<h3 class="h3">Available Credits For This Course </h3>
		<?php $i = 0; ?>	
		<div class="row">
		@foreach($course->credit as $credit )
			@if($credit->active)
			    <?php $file = $page->getFile($credit->media_id) ?>
				<div class="col-sm-4">
					<div class="panel panel-default ">
						<div class="panel-heading">{{ $credit->title }}</div>
						<div class="panel-body" style="text-align: justify;" >
						{{ ucfirst($credit->summary) }}
						</div>
						<div class="panel-footer">
							<a href="{{ asset(url($file)) }}" target="__blank" class="btn btn-md btn-link pull-right">Read More</a>
							<br>
						</div>
					</div>
				</div>
			@endif
		@endforeach
		</div>
	</div>
</div>
<div class="clear-fix">&nbsp;</div>
@endsection

@section('styles')

@endsection

@section('scripts_t')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/1.5.458/pdf.min.js"></script>
<script type="text/javascript">
	PDFJS.workerSrc = "https://cdnjs.cloudflare.com/ajax/libs/pdf.js/1.5.458/pdf.worker.min.js";
</script>
@endsection

@section('scripts')
<script type="text/javascript">
</script>
@endsection