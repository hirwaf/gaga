@extends('layouts.app')

@section('title','Statistics')

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2"> 
		@if( $statistic->count() > 0 )
		@foreach( $statistic as $key => $stat )
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading{{ $key }}">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $key }}" aria-controls="collapse{{ $key }}">
						 Credit :{{ $repo->getCredit($stat->credit_id) }}
						</a>
						<small class="pull-right">Date:{{ $stat->created_at->diffForHumans() }}</small>
					</h4>
				</div>
				<div id="collapse{{ $key }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{ $key }}">
					<div class="panel-body">
						<h5 class="label label-primary">Course : {{ $repo->getCourse($stat->course_id) }}</h5>
						<div class="clearfix">&nbsp;</div>
						<table class="table">
							@if( count( $repo->json($stat->question_id) ) )
									<tr>
										<th>#</th>
										<th colspan="2">Question</th>
										<th>Your Answer</th>
										<th>Decision</th>
									</tr>
								<?php $i = 0; foreach ($repo->json($stat->question_id) as $key => $question):?>
									<tr>
										<td>{{ ++$i }}</td>
										<td colspan="2">
											{{ $repo->getQuestion( $question ) }}
										</td>
										<td> {{ $repo->getAnswer($repo->json($stat->answered)[$key]) }} </td>
										<td>
											{!! $repo->json($stat->collect)[$key] ? "<label class='label label-success'>Correct</label>" : "<label class='label label-danger'>Incorrect</label>" !!}
										</td>
									</tr>
								<?php endforeach; ?>
									<tr>
										<td colspan="5">
											<label class="label {{ $repo->marks($stat->id)[1] }} pull-right" style="font-weight: 500;font-size: large;">
												{{ $repo->marks($stat->id)[0] }}
											</label>
										</td>
									</tr>
							@endif
						</table>
					</div>
				</div>
			</div>
		</div>
		@endforeach
		@else
		<h1 class="text-center">You don't have any Statistic</h1>
		@endif
		</div>
	</div>
</div>	
<div class="clear-fix">&nbsp;</div>
@endsection

@section('styles')

@endsection

@section('scripts')
<script type="text/javascript">
	$(function(){
		$('form ').addClass(' form ');
		$('* input ').addClass(' form-control ');
		$('* textarea ').addClass(' form-control ');
	});
</script>
@endsection
