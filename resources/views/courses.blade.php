@extends('layouts.app')

@section('title',$page->title)

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<h1>All Our Courses and Credits</h1>
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
		@foreach($page->content as $course )
			<table class="table col-xs-12 col-sm-12 col-md-12 col-lg-12 log" >
				<tbody class="" >
					<tr>
					    <tr> <a href="{{ url(route('course.show',$course->course)) }}"><h3>{{ ucfirst($course->course) }}</h3></a> </tr>
						<td class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
							<p>
								{{ ucfirst($course->summary )}}
							</p>
						</td>
					</tr>	
				</tbody>
			</table>
		@endforeach
		
	</div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<center>{!! $page->content->links() !!}</center>
	</div>
</div>
<div class="clear-fix">&nbsp;</div>
@endsection

@section('styles')

@endsection

@section('scripts')
<script type="text/javascript">
</script>
@endsection