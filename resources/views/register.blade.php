<?php
	function geterrors(){
		
		if(session()->has('errors')){
			$errors = session('errors');
			return $errors;
		}
		else
			return [];
	}

	function inputs($name = ''){
		// $oldInputs = @session()->pull('with')['oldInputs'];
		// if(isset($oldInputs)){
		// 	$input = $oldInputs[$name];
		// 	return $input;
		// }
		// else
			return "";
	}
?>
@extends('layouts.app')

@section('title',$page->title)

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
	<div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
			<h1>Register now to start your courses</h1>
	</div>
	<form role='form' action="{{ url(route('registration.store')) }}" method="post" enctype="multipart/form-data"  >
		<div class=" col-xs-12 col-sm-12 col-md-7 col-lg-7 responsive-table">
			{{ csrf_field() }}
				@if( geterrors() )
					@foreach( geterrors() as $error )
						<div class="alert alert-danger alert-dismissible">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							{{ $error }}
						</div>
					@endforeach
				@endif
			 	<table class="table col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
					<tbody>
						<tr>
							<td>Your First name</td>
							<td>
								<input type="text" required autofocus name="firstname" value="{{ old('firstname') }}" class="form-control col-sx-12 col-sm-12 col-md-8 col-lg-8 inputs">

							</td>
						</tr>
						<tr>
							<td>Your Last name</td>
							<td>
								<input type="text" required name="lastname" value="{{ old('lastname') }}" class="form-control col-sx-12 col-sm-12 col-md-8 col-lg-8 inputs">

							</td>
						</tr>
						<tr>
							<td>Sex</td>
							<td>
								<label class="checkbox-inline">
									<input type="radio" value="Male" name="gender"  checked>Male
								</label>
								<label class="checkbox-inline">
									<input type="radio" value="Female" name="gender">Female
								</label>
							</td>
						</tr>
						<tr>
							<td>Districts</td>
							<td>
								<select required class="form-control district" name="district">
									<option value="" disabled selected >Select Your District...</option>
									@foreach(\App\District::orderBy('district_name','asc')->get() as $sector )
									<option value="{{ $sector->id }}" > {{ $sector->district_name }} </option>
									@endforeach
								</select>
								<span class="text text-sm text-info">Use Mouse to select district</span>
							</td>
						</tr>
						<tr id="sector">
							<td>Sectors</td>
							<td>
								<select required class="form-control sector" name="sector" ></select>
							</td>
						</tr>
						<tr>
							
						</tr>
						<tr>
							<td>Your E-mail</td>
							<td><input type="email" required autocomplete="off" name="email" value="{{ old('email') }}" class="form-control col-sx-12 col-sm-12 col-md-8 col-lg-8 inputs"></td>
						</tr>
						<tr>
							<td>Create your password</td>
							<td><input type="password" required name="password"  class="form-control col-sx-12 col-sm-12 col-md-8 col-lg-8 inputs"></td>
						</tr>
						<tr>
							<td>Confirm your password</td>
							<td><input type="password" required name="password_confirmation" class="form-control col-sx-12 col-sm-12 col-md-8 col-lg-8 inputs"></td>
						</tr>
						<tr>
							<td class="col-sx-12 col-sm-12 col-md-3 col-lg-3">Your Proffessional</td>
							<td><input type="text" required name="proffessional" value="{{ old('proffessional') }}" class="form-control col-sx-12 col-sm-12 col-md-8 col-lg-8 inputs"></td>
						</tr>
						
						<tr>
							<td>Your Phone Number</td>
							<td><input type="text" required autocomplete="off" maxlength="10" minlength="10" value="{{ old('telephone') }}"  name="telephone" class="form-control col-sx-12 col-sm-12 col-md-8 col-lg-8 inputs"></td>
						</tr>
					</tbody>
				</table>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 responsive-table">
			<table class="table col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
				<tbody>
					<tr>
						<td>Passport Photo</td>
						<td><input type="file" name="picture" id="filer_input" class="col-sx-12 col-sm-12 col-md-8 col-lg-8"></td>
					</tr>
					<tr>
						<td>Diplome</td>
						<td><input type="file" name="cv[]" id="filer_input2" multiple="multiple" class="col-sx-12 col-sm-12 col-md-8 col-lg-8"></td>
					</tr>
				</tbody>
			</table>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 click page-header" >
				<button type="submit" class="btn btn-default register">Accept Registraion</button>
				<button type="reset" class="btn btn-default register pull-right">Cancel Registration</button>
			</div>
		</div>
	</form>
@endsection
<?php session()->forget('errors');  ?>
@section('styles')
<link rel="stylesheet" href="{{asset(url('plugins/jQuery.filer/css/jquery.filer.css'))}}" rel="stylesheet" />
<link rel="stylesheet" href="{{asset(url('css/site.plugins.css'))}}" rel="stylesheet" />
@endsection

@section('scripts')
<script src="{{ asset(url('plugins/jQuery.filer/js/jquery.filer.min.js?v=1.0.5')) }}"></script>  
<script src="{{ asset(url('js/register.js?v=1.0.5')) }}"></script>

<script type="text/javascript">
  $('#sector').hide();
  $('.district').on('click',function(e){
  	var v = $(this).val();
  	$.ajax({
  		url : '/getsector',
  		type : 'POST',
  		beforeSend : function(xhr){
  			var token = "{{ csrf_token() }}";
  			if(token){
  				return xhr.setRequestHeader('X-CSRF-TOKEN',token);
  			}
  		},
  		data : {
  			district : v
  		},
  		success : function(data){
  			$('.sector').html(data);
  			$('#sector').fadeIn();
  		},
  		error : function(error){
  			console.log(error);
  		}
  	});
  });
</script>
@endsection