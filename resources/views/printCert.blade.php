<!DOCTYPE html>
<html>
<head>
      <title>Certificate of Completion<</title>
      <script src="{{ asset(url('plugins/jQuery/jQuery-2.2.0.min.js')) }}"></script>
      <script src="{{ asset(url('plugins/jQuery.Print/jQuery.print.js')) }}"></script>
      <script type="text/javascript">
          $(function() {
            $("#print").on('click', function() {
               $("#printable").print({
                  // globalStyles : true,
                  title : "E-PHYSIO"
                  // mediaPrint : true,
               });
            });
          });
      </script>
      <style type="text/css">
            @media print{
                  #printable{
                        background-color: #f5f5f5;                       
                  }
            }
      </style>
</head>
<body>
      <div style="background-color: #f5f5f5;width:800px; height:auto; padding:20px; text-align:center; border: 10px solid #787878" id="printable">
      <div style="width:750px; height:auto; padding:20px; text-align:center; border: 5px solid #787878">
             <span style="font-size:50px; font-weight:bold">Certificate of Completion</span>
             <br><br>
             <span style="font-size:25px"><i>This is to certify that</i></span>
             <br><br>
             <span style="font-size:30px"><b>{{ $user }}</b></span><br/><br/>
             <span style="font-size:25px"><i>has completed the course</i></span> <br/><br/>
             <span style="font-size:30px">{{ $course }}</span> <br/><br/>
             <span style="font-size:20px">with score of <b>{{$grade}}%</b></span> <br/><br/><br/><br/>
             <span style="font-size:25px"><i>dated</i></span><br>
            {{ $date }}
            <!-- <span style="font-size:30px">$dt</span> -->
            <div style="margin-top: 1em;">
                  <img src="{{ asset(url('img/sign.png')) }}">
            </div>
      </div>
      </div>
      <div id="print" style="cursor: pointer; background-color: blue; color: white; padding: 1em; width: 5em; text-align: center;">Print</div>
</body>
</html>
