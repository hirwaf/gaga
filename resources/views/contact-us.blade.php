@extends('layouts.app')

@section('title',"Contact Us")

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
<div class="row">
	<form class="form-horizontal" action="{{ url(route('contact.us.post.web')) }}" method="post">
	{!! csrf_field() !!}
		<div class="col-sm-12">
			@if (session()->has('errors'))
			    <!-- Form Error List -->
			    <div class="alert alert-danger">
			        <strong>Whoops! Something went wrong!</strong>
			        <br><br>
			        <ul style="list-style: none;">
			            @foreach (session()->pull('errors') as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@elseif(session()->has('success'))
			    <div class="alert alert-success">
			        <strong>{{ session()->pull('success') }}</strong>
			        <br>
			    </div>
			@endif
		</div>
		<div class="form-group">
			<label for="inputName" class="col-sm-2 control-label">Name</label>
			<div class="col-sm-7">
			  <input type="text" name="name" autofocus class="form-control" id="inputName" placeholder="Your Names" value="{{ old('name') }}" required>
			</div>
		</div>
		<div class="form-group">
			<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
			<div class="col-sm-7">
			  <input type="email" name="email" class="form-control" id="inputEmail3" placeholder="Email" value="{{ old('email') }}" required>
			</div>
		</div>
		<div class="form-group">
			<label for="message" class="col-sm-2 control-label">Message</label>
			<div class="col-sm-7">
			  <textarea class="form-control" rows="3" name="message" id='message' placeholder="Your Suggestion, Problem or Opinion ">{{ old('message') }}</textarea>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-7">
			  <button type="submit" class="btn btn-primary">Send My Message</button>
			</div>
		</div>
	</form>	
</div>
<div class="clear-fix">&nbsp;</div>
@endsection

@section('styles')

@endsection

@section('scripts')
<script type="text/javascript">
	$(function(){
		$('form ').addClass(' form ');
		$('* input ').addClass(' form-control ');
		$('* textarea ').addClass(' form-control ');
	});
</script>
@endsection