<?php
  // Pagination Section
  $p = isset($_GET['p'])? $_GET['p'] : '1';

  $images = $page->content;
  $start = $p == 1 ? 0 : ($page->perpage - 1) + ($p - 1) ;
  $end =  $start + ($page->perpage - 1);
  $count = $page->all;
  $end = $end <= $count ? $end : $count - 1;
  $ko = isset($_GET['tk'])? '&tk='.$_GET['tk'] : '';

  //Token for delete images
  $token = "";  
  if(isset( $_GET['tk'] ))
    $token = $_GET['tk']
?>
@extends('layouts.app')

@section('title',$page->title)

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
<section class="gallery-section">
  @if(\Auth::guard('admins')->check() && \Session::has('del-gal') && \Session::get('del-gal') === $token )
  <div class="row del-btn hidden ">
    <div class=" col-md-offset-5">
      <button class="btn btn-danger btn-md btn-flat" >Delete Selected Images</button>
    </div>
    <div class="clearfix">&nbsp;</div>
  </div>
  @endif
	<ul class="row">
    @if( isset($images[$end]) && $p <= $page->pages )
      @foreach(range($start, $end) as $value )
            @if( \File::exists( public_path().'/img/gallery/'.$images[$value] ) )
              <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
                  <img class="img-responsive" src="{{ asset(url('/img/gallery/'.$images[$value])) }}">
                  @if(\Auth::guard('admins')->check() && \Session::has('del-gal') && \Session::get('del-gal') === $token )
                    <!-- <form action="{{ url(route('gallery.destroy')) }}"  method="post" class="form-destroy" > -->
                      <button type="button" class="btn btn-flat btn-block btn-xs btn-danger delete-image" value="{{ $images[$value] }}" style="margin-top: 3px;border-radius: 0px;" >Delete <span class="glyphicon glyphicon-arrow-up" style="font-weight: normal;font-size: xx-small;"></span></button>
                      <!-- <input type="checkbox" name ="images[]" value="{{ $images[$value] }}" class="pull-right checkbox">
                      <input type="hidden" name="tk" value="{{ $token }}" >
                      <input type="hidden" name="checkbox" value="1" > -->
                    <!-- </form> -->
                  @endif
              </li>
            @endif
      @endforeach
    @endif
	</ul>
  @if($page->pages > 1  )
	<nav>
	  <ul class="pager">
	    @if( $p > 1 )
        <li><a href="/gallery?p={{ ($p - 1).$ko  }}">Previous</a></li>
      @endif
      @if($page->pages > $p )
	     <li><a href="/gallery?p={{ ($p + 1).$ko  }}">Next</a></li>
      @endif
	  </ul>
	</nav>
  @endif
	<div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">         
          <div class="modal-body">                
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</section>
<div class="clear-fix">&nbsp;</div>
@endsection

@section('styles')
  <style>
      .gallery-section ul {         
          padding:0 0 0 0;
          margin:0 0 0 0;
      }
      .gallery-section ul li {     
          list-style:none;
          margin-bottom:25px;           
      }
      .gallery-section ul li img {
          cursor: pointer;
          box-shadow: 0px 0px 2px #999;
      }
      .modal-body {
          padding:5px !important;
      }
      .modal-content {
          border-radius:0;
      }
      .modal-dialog img {
          text-align:center;
          margin:0 auto;
      }
    .controls{          
        width:50px;
        display:block;
        font-size:11px;
        padding-top:8px;
        font-weight:bold;          
    }
    .next {
        float:right;
        text-align:right;
    }
      /*override modal for demo only*/
      .modal-dialog {
          max-width:500px;
          padding-top: 90px;
      }
      @media screen and (min-width: 768px){
          .modal-dialog {
              width:500px;
              padding-top: 90px;
          }          
      }
      @media screen and (max-width:1500px){
          #ads {
              display:none;
          }
      }
  </style>
@endsection

@section('scripts')
<script type="text/javascript" src='{{ asset(url("/plugins/gallery/photo-gallery.js")) }}' ></script>
<script type="text/javascript">
  $(function(){
    $('*.delete-image').on('click',function(e){
        var v = e.target.value;
        $.ajax({
          url  : "{{ url(route('gallery.destroy')) }}",
          type : 'POST',
          beforeSend : function(xhr){
            var token = "{{ csrf_token() }}";
            if(token){
              return xhr.setRequestHeader('X-CSRF-TOKEN',token);
            }
          },
          data : {
            image : v,
            checkbox : '0',
            tk : "{{ isset($token)? $token : '' }}"
          },
          success : function(data){
            //alert(data);
            window.location = "{{ url(route('gallery.index')) }}?p={{ $p }}";
            window.close();
          },
          error : function(error){
            console.log(error);
          }
        });
    });
    $('.checkbox').on('click', function(i){
      $('.del-btn').removeClass('hidden');
    });
    $('.del-btn').on('click', function(i){
      $('.form-destroy').submit();
    });
  });
</script>
@endsection