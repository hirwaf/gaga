<?php  
  $errorP = false;
  if( Session::has('error') ){
      $errorP = Session::get('error');
      Session::forget('error');
  }
  function display($name,$siteinfo){
    if( $siteinfo->siteinfo($name) )
      return $siteinfo->siteinfo($name);
    else 
      return "";
  }
  function olds($olds,$name){
    if($olds != "")
      return $olds[$name];
    else 
        return false;
  }
?>
@extends('layouts.app_admin')
@section('title',"Web Settings | $admin->name")
@section('menu')
    @include('auth.layouts.super.menu')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ $title }}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">{{ $short }}</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="box box-solid">
            <!-- form start -->
            <form class="form-horizontal" role="form" method="post" action='{{ url(route($url)) }}' enctype="multipart/form-data" >
            {{ csrf_field() }}
              <div class="box-body">
                <div class="clearfix">&nbsp;</div>
                <div class="flash-message">
                    <?php $mm = ''; ?>
                    @foreach(['danger', 'warning', 'success', 'info'] as $msg)
                        <?php
                            if( Session::has('alert-' . $msg) ){
                                $mm = 'alert-'.$msg;
                                $m = Session::get('alert-' . $msg);
                                Session::forget('alert-' . $msg);
                            }
                        ?>
                        @if( $mm == ('alert-'.$msg) )
                            <p class="alert alert-{{ $msg }}">
                                {{ $m }}
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                        @endif
                    @endforeach
                </div>
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Site Title</label>
                    <div class="col-sm-10">
                      <input type="text" id="title" name="siteTitle" class="form-control" autocomplete="off" value="{{ display('sitetitle',$siteinfo) }}" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="desc" class="col-sm-2 control-label">Tagline</label>
                    <div class="col-sm-10">
                      <input type="text" id="title" name="siteTagLine" class="form-control" autocomplete="off" value="{{ display('tagline',$siteinfo) }}" >
                      <span class="text-sm text-quo"><i>In a few words, explain what this site is about.</i></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email Address</label>
                    <div class="col-sm-10">
                      <input type="email" id="email" name="siteEmail" class="form-control" autocomplete="off" value="{{ display('email',$siteinfo) }}" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">Telephone</label>
                    <div class="col-sm-10">
                      <input type="text" id="phone" name="phone1" class="form-control" data-inputmask='"mask": "250#########" ' data-mask autocomplete="off" value="{{ display('phone1',$siteinfo) }}" >
                      <input type="text" id="phone" name="phone2" class="form-control" data-inputmask='"mask": "250#########" ' data-mask autocomplete="off" value="{{ display('phone2',$siteinfo) }}" >
                      <input type="text" id="phone" name="phone3" class="form-control" data-inputmask='"mask": "250#########" ' data-mask autocomplete="off" value="{{ display('phone3',$siteinfo) }}" >
                      <input type="text" id="phone" name="phone4" class="form-control" data-inputmask='"mask": "250#########" ' data-mask autocomplete="off" value="{{ display('phone4',$siteinfo) }}" >
                      <input type="text" id="phone" name="phone5" class="form-control" data-inputmask='"mask": "250#########" ' data-mask autocomplete="off" value="{{ display('phone5',$siteinfo) }}" >
                    </div>
                </div>
                <div class="form-group">
                  <label for="map" class="col-sm-2 control-label">Google Map</label>
                  <div class="col-sm-10">
                    <input type="text" name="map" class="form-control" value="{{ display('map',$siteinfo) }}" >
                  </div>
                </div>
                <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">Social Media</label>
                    <div class="col-sm-10">
                      <div class="input-group margin">
                        <div class="input-group-btn">
                          <a href="{{ display('facebook',$siteinfo) }}" target="__blank" class="btn btn btn-social-icon btn-facebook">
                            <i class="fa fa-facebook"></i>
                          </a>
                        </div>
                        <!-- /btn-group -->
                        <input type="text" name="facebook" class="form-control" value="{{ display('facebook',$siteinfo) }}" >
                      </div>
                      <div class="input-group margin">
                        <div class="input-group-btn">
                          <a href="{{ display('twitter',$siteinfo) }}" target="__blank" class="btn btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
                        </div>
                        <!-- /btn-group -->
                        <input type="text" name="twitter" class="form-control" value="{{ display('twitter',$siteinfo) }}" >
                      </div>
                      <div class="input-group margin">
                        <div class="input-group-btn">
                          <a href="{{ display('googleplus',$siteinfo) }}" target="__blank" class="btn btn-social-icon btn-google"><i class="fa fa-google-plus"></i></a>
                        </div>
                        <!-- /btn-group -->
                        <input type="text" name="googleplus" class="form-control" value="{{ display('googleplus',$siteinfo) }}" >
                      </div>
                      <div class="input-group margin">
                        <div class="input-group-btn">
                          <a href="{{ display('linkedin',$siteinfo) }}" target="__blank" class="btn btn-social-icon btn-linkedin"><i class="fa fa-linkedin"></i></a>
                        </div>
                        <!-- /btn-group -->
                        <input type="text" name="linkedin" class="form-control" value="{{ display('linkedin',$siteinfo) }}" >
                      </div>                      
                    </div>
                </div>
                <div class="form-group">
                  <label for="icon" class="col-sm-2 control-label">Icon</label>
                  <div class="col-sm-8">
                    <input type='file' name='icon' id="icon" class='form-control' title="Upload Image File">
                    <span class="text-sm text-info">Upload an image file</span>  
                  </div>
                  <div class="col-sm-1" style="height: 2em; background-color: transparent;border-radius: 0.1em; " >
                    <img src="{{ asset(url('/img/icon/'.display('icon',$siteinfo))) }}" class="img img-responsive" >
                  </div>
                </div> 
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <div class="col-sm-7 col-sm-offset-3">
                  <input required type="password" name='password' class="form-control" id="inputPassword3" placeholder="Password">
                  <span class="help-block info" style='font-size: x-small;' >Please fill in your password, to save changes.</span>
                </div>
                <div class="clearfix"></div>
                <button type="submit" class="btn btn-info btn-md col-sm-3 col-md-offset-5">{{ $button }}</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        </div> 
      </div>
    </section>
    <!-- /.content -->
  </div>
  @if ( $errorP )
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel2">Error Find</h4>
          </div>
          <div class="modal-body">
              <h3>Incorrect Password !!</h3>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  @endif
@endsection
@section('scripts_top')

<script type="text/javascript">
    $(function(){
        $('.bs-example-modal-sm').modal();
    });
</script>

@endsection
@section('script')
@endsection