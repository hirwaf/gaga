<?php  
  $errorP = false;
  if( Session::has('error') ){
      $errorP = Session::get('error');
      Session::forget('error');
  }
?>
@extends('layouts.app_admin')
@section('title',"All Menu  | $admin->name ")
@section('style_p')
  <link rel="stylesheet" href="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset(url('plugins/datatables/dataTables.bootstrap.css')) }}">
@endsection
@section('styles')
<style type="text/css">
  .on{
    text-align: left;
    background: #16A085;
    color: #fff;
    cursor: pointer;
  }
  .on .fa ,.off .fa{
    padding-top: .25em; 
  }
  .on:hover{
    color: #fff;
  }
  .off:click{
    box-shadow: none;
  }
  .off{
    text-align: right;
    cursor: none;
    color: #fff;
    background-color: #34495E; 
  }
</style>
@endsection
@section('menu')
    @include('auth.layouts.super.menu')
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Menu
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">pages</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="box box-solid">
            <div class="box-body">
              <table id="example2" class="table table-hover text-center">
                    <thead>
                    <tr>
                      <th>Order</th>
                      <th>Menu Name</th>
                      <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                       <?php $i= 1; ?>  
                       @foreach($menus as $menu)
                        <tr>
                          <td>
                            <button class="btn btn-sm btn-flat btn-primary order-menu" style="cursor: pointer;" value="{{ $i }}" >{{ $menu->order  }} </button>
                          </td>
                          <td>{{ $menu->page->name }}</td>
                          <td>
                            <button class="btn btn-sm btn-flat btn-{{ $menu->active ? 'primary' : 'danger' }} active-menu" style="cursor: pointer;" value="{{ $i }}" >Is Active : {{ $menu->active ? 'Yes' : 'No' }} </button>
                          </td>
                        </tr>
                        <?php 
                        changeOrder($i,$menu->page->name,$menu->id,$menus, $menu->order); 
                        activation($i,$menu->page->name,$menu->id,$menu->active,$menu->auth,$menu->only);
                        ?>
                        <?php $i++;  ?>
                       @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                          <th>Order</th>
                          <th>Menu Name</th>
                          <th>Status</th> 
                      </tr>
                    </tfoot>
              </table>  
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection
@if ( $errorP )
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel2">Error Find</h4>
        </div>
        <div class="modal-body">
            <h3>Incorrect Password !!</h3>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endif
@section('script_p')
  <!-- DataTables -->
  <script src="{{ asset(url('plugins/datatables/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(url('plugins/datatables/dataTables.bootstrap.min.js')) }}"></script>
  <!-- FastClick -->
  <script src="{{ asset(url('plugins/fastclick/fastclick.js')) }}"></script>
@endsection
@section('script')
  <script type="text/javascript">
    $(function(){
      $('.bs-example-modal-sm').modal();
         
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
      });
//      $('.btn-delete').on('click',function(e){
//          var b = e.target.value;
//      });    
      $('* .order-menu').on('click',function(e){
        var b = e.target.value;
        $('.bs-modal-c-'+b).modal();
      });
      $('* .active-menu').on('click',function(e){
        var b = e.target.value;
        $('.bs-modal-a-'+b).modal();
      });
    });
  </script>
@endsection
@section('scripts_top')
@endsection
<?php
function changeOrder($i,$title,$id,$menus,$was = 1){
?>
<div class="modal modal-primary fade modal-top-20 bs-modal-c-{{ $i }}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">
          <span id="">Change Queue Ordering of </span>&nbsp;
          <span style="text-transform: uppercase;">{{ $title }}</span> 
        </h4>
      </div>
      <div class="modal-body">
          <form action="{{ url(route('order.menu.super')) }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="menu" value="{{ $id }}" >
            <div class="form-group">
              <select class="form-control" name='order' required >
                @for($p = 1; $p <= $menus->count(); $p++ )
                  <option value='{{ $p }}' {{ $was == $p ? "selected" : "" }} > {{ $p }} </option>
                @endfor
              </select>
            </div>
            <div class="form-group">
              <input required type="password" autofocus name='password' class="form-control border-none radius-2 passwordS" id="inputPassword3" placeholder="Password">
              <span class="help-block info" style='font-size: x-small;color: #fff;' >Please fill in your password and press ENTER, to delete this page.</span>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
<?php  
}
function activation($i,$title,$id,$was,$vl=0,$ol=0){
?>
<div class="modal modal-{{ $was ? 'danger' : 'primary' }} fade modal-top-20 bs-modal-a-{{ $i }}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">
          <span id=""> {{ $was ? "Disable" : "Enable" }} </span> 
          <span style="text-transform: capitalize;font-style: italic;">{{ $title }}</span> ? 
        </h4>
      </div>
      <div class="modal-body">
          <form action="{{ url(route('status.menu.super')) }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="menu" value="{{ $id }}" >
            <input type="hidden" name='active' value="{{ $was ? false : true }}">
            <div class="form-group">
              <label class='label-control'>Visible When Login
                <input type='checkbox' name='onauth' {{ $vl? "checked" : "" }} value='y'  >
              </label>
            </div>
            <div class="form-group">
              <label class='label-control'>Visible Only When Login
                <input type='checkbox' name='onlyauth' {{ $ol? "checked" : "" }} value='y' >
              </label>
            </div>
            <div class="form-group">
              <input required type="password" autofocus name='password' class="form-control border-none radius-2 passwordS" id="inputPassword3" placeholder="Password">
              <span class="help-block info" style='font-size: x-small;color: #fff;' >Please fill in your password and press ENTER</span>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
<?php  
}
?>