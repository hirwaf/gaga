<?php  
  $errorP = false;
  if( Session::has('error') ){
      $errorP = Session::get('error');
      Session::forget('error');
  }
?>
@extends('layouts.app_admin')
@section('title',"All Locations  | $admin->name ")
@section('style_p')
  <link rel="stylesheet" href="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset(url('plugins/datatables/dataTables.bootstrap.css')) }}">
@endsection
@section('styles')
<style type="text/css">
  .cvs{
    padding: 0px;
    margin: 0px;
  }
  .cvs li {
    float: left;
  }
  .on{
    text-align: left;
    background: #16A085;
    color: #fff;
    cursor: none;
  }
  .on .fa ,.off .fa{
    padding-top: .25em; 
  }
  .on:hover{
    color: #fff;
  }
  .off:click{
    box-shadow: none;
  }
  .off{
    text-align: right;
    cursor: none;
    color: #fff;
    background-color: #34495E; 
  }
</style>
@endsection
@section('menu')
    @include('auth.layouts.super.menu')
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Client <a href="{{ url(route('all.user.super')) }}" class="btn btn-sm btn-warning">Go Back</a>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="">View</li>
        <li class="active">User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="box">
            <div class="box-body">
                <div class="row">
                  <div class="col-md-4">
                    <img src="{{ $userrepo->getProfile() }}" alt="{{ $user->firestname.' '.$user->lastname }}" class="img-responsive img-rounded">
                  </div>
                  <div class="col-md-8" style="padding: 0px;">
                    <div class="row" style="padding: 0px;">
                      <div class="col-md-11">
                        <table class="table table-bordered">
                          <tr>
                            <td align="right">Names</td>
                            <td><b>{{ ucfirst($user->firstname." ".$user->lastname) }}</b></td>
                          </tr>
                          <tr>
                            <td align="right">Gender</td>
                            <td><b>{{ ucfirst($user->sex) }}</b></td>
                          </tr>
                          <tr>
                            <td align="right">Telephone</td>
                            <td><b>{{ ($user->telephone{0} != '0'? "0": "").$user->telephone }}</b></td>
                          </tr>
                          <tr>
                            <td align="right">Email</td>
                            <td><b>{{ $user->email }}</b></td>
                          </tr>
                          <tr>
                            <td align="right">Proffessional</td>
                            <td><b>{{ $userrepo->getProffessional() }}</b></td>
                          </tr>
                          <tr>
                            <td align="right">Address</td>
                            <td><b>{{ ucfirst($userrepo->getAddress()) }}</b></td>
                          </tr>
                          <tr>
                            <td align="right">Approve</td>
                            <td>
                              <?= $user->approved != 1? "<button class='btn btn-sm btn-info payed' value='".$user->id."' >Payed</button>" : "<div class='label label-primary '>Clear</div>" ?>
                            </td>
                          </tr>
                        </table>
                        <?php modalDelete($user->id,'',$user->id); ?>
                      </div>                      
                    </div>
                  </div>
                </div>
                <div class="row" style="margin-bottom: 10px;">
                  <div class="col-md-12">
                    @php
                        $cvs = $userrepo->getCv();
                        $i = 0;
                    @endphp
                    <h4>Uploaded Curriculum vitae: {{ count($cvs) }}</h4>
                    @if(count($cvs) > 0)
                        <ul class='cvs'>
                          @foreach($cvs as $cv)
                              <li class='btn btn-sm btn-success cv' data-url = "{{ $userrepo->getCVPath($cv) }}">cv: {{ ++$i }}</li>
                          @endforeach
                        </ul>
                    @endif
                  </div>
                  <div class="col-md-12" id='pdf_view' style="margin-top: 10px;"></div>
                </div>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script_p')
  <!-- DataTables -->
  <script src="{{ asset(url('plugins/datatables/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(url('plugins/datatables/dataTables.bootstrap.min.js')) }}"></script>
  <!-- FastClick -->
  <script src="{{ asset(url('plugins/fastclick/fastclick.js')) }}"></script>
@endsection
@section('script')
  <script type="text/javascript">
    $(function(){
      $('.payed').on('click',function(e){
        var b = e.target.value;
        $("*#modalTitle").text('Yes this person has payed');
        $('.bs-modal-'+b).modal();
        $("*#what").attr('value',function(){
          return "0";
        });
      });
      
      $("*.cv").on('click', function(){
        var url = $(this).data('url');
        $obj = $('<object>');
        $obj.attr("data",url);
        $obj.attr("pluginspage", "http://www.adobe.com/products/acrobat/readstep2.html");
        $obj.attr("alt","pdf");
        $obj.attr("width", "100%");
        $obj.attr("height", "1000px");
        //$obj.attr("type","application/pdf");
        //$obj.addClass("w100");

        $("#pdf_view").html($obj);
        
      });
    });
  </script>
@endsection
@section('scripts_top')
@endsection
<?php
function modalDelete($i,$title,$id){
?>
<div class="modal modal-primary fade modal-top-20 bs-modal-{{ $i }}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">
          <span id="modalTitle"></span>&nbsp;
          <span style="text-transform: uppercase;">{{ $title }}</span> 
        </h4>
      </div>
      <div class="modal-body">
          <form action="{{ url(route('allow.user.super')) }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name='what' style="color:#444;" id='what' >
            <input type="hidden" name="client" value="{{ $id }}" >
            <div class="form-group">
              <input required type="password" focus='true' name='password' class="form-control border-none radius-2 passwordS" id="inputPassword3" placeholder="Password">
              <span class="help-block info" style='font-size: x-small;color: #fff;' >Please fill in your password and press ENTER, to allow this user.</span>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
<?php  
}
?>