<?php
    function activeMenu($values){
        $route = Route::current();
        $nameRoute = $route->getName();
        for($x = 0;$x < count($values);$x++) {

        	if($values[$x] == $nameRoute)
            	return "active";
        }
        return "";
    }
    $admin = new \App\Admin;
    $profile = activeMenu( ['profile.super'] );
    $pages = activeMenu( ['all.pages.super','create.page.super','edit.page.super','all.subpages.super','create.subpage.super','edit.subpage.super','menu.page.super'] ); 
    $subpages = activeMenu( ['all.subpages.super','create.subpage.super'] ); 
    $users = activeMenu(['all.user.super','add.user.super','allowed.user.super','not.allowed.user.super']);
    $option = activeMenu(['all.options.super','create.option.super']);
    $courses = activeMenu(['all.courses.super','create.course.super','edit.course.super']);
    $credits = activeMenu(['all.credits.super','create.credit.super','edit.credit.super','all.questions.super','create.question.super','edit.question.super']);
    $questions = activeMenu(['all.questions.super','create.question.super','edit.question.super']);
    $media = activeMenu(['all.media.super','create.media.super','edit.media.super']);
    $sub = activeMenu(['subcribes.index']);
?>

<li class="{{ $pages }} treeview">
    <a href="#">
        <i class="fa fa-external-link"></i> <span>Pages</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ url(route('all.pages.super')) }}"><i class="fa fa-sort-alpha-desc"></i> All Pages</a></li>
        <li><a href="{{ url(route('menu.page.super')) }}"><i class="fa fa-reorder"></i> Site Menu</a></li> 
        <?php if($admin->isHe()): ?>
        <li><a href="{{ url(route('create.page.super')) }}"><i class="fa fa-user-plus"></i> Add Page</a></li>           
        <li class="{{ $subpages }}">
            <a href="{{ url(route('create.subpage.super')) }}">
                <i class="fa fa-external-link"></i> <span>Add Subpage</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
        </li>
        <?php endif ?> 
    </ul>
</li>
<li class="{{ $courses }} treeview">
    <a href="#">
        <i class="fa fa-paragraph"></i> <span>Courses</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ url(route('all.courses.super')) }}"><i class="fa fa-sort-alpha-desc"></i>Courses List</a></li>
        <li><a href="{{ url(route('create.course.super')) }}"><i class="fa fa-user-plus"></i> New Course</a></li>              
    </ul>
</li>
<li class="{{ $credits }} treeview">
    <a href="#">
        <i class="fa  fa-file-text-o"></i> <span>Credits</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ url(route('all.credits.super')) }}"><i class="fa fa-sort-alpha-desc"></i> All Credits</a></li>
        <li><a href="{{ url(route('create.credit.super')) }}"><i class="fa fa-user-plus"></i> New Credit</a></li>            
        <li class="{{ $questions }}">
            <a href="#">
                <i class="fa  fa-question"></i> <span>Questions</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="{{ url(route('all.questions.super')) }}"><i class="fa fa-sort-alpha-desc"></i> All Questions</a></li>
                <li><a href="{{ url(route('create.question.super')) }}"><i class="fa fa-user-plus"></i> New Question</a></li>            
            </ul>
        </li>    
    </ul>
</li>

<li class="{{ $users }} treeview">
    <a href="#">
        <i class="fa fa-users"></i> <span>Clients</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ url(route('all.user.super')) }}"><i class="fa fa-sort-alpha-desc"></i>Clients list</a></li>
        <li><a href="{{ url(route('not.allowed.user.super')) }}"><i class="fa fa-hand-paper-o"></i>Requested</a></li>            
        <li><a href="{{ url(route('allowed.user.super')) }}"><i class="fa fa-thumbs-o-up"></i>Allowed</a></li>            
    </ul>
</li>

<li class=" {{ $media }} treeview hidden " ><a href="{{ url(route('all.media.super')) }}"><i class="fa  fa-arrows-alt"></i> <span>Media</span></a></li>
<li class=" {{ $option }} treeview" ><a href="{{ url(route('all.options.super')) }}"><i class="fa  fa-bars"></i> <span>Settings</span></a></li>


<li class=" {{ $sub }} treeview" ><a href="{{ url(route('subcribes.index')) }}"><i class="fa fa-user"></i> <span>Subcribe</span></a></li>
<li class=" {{ $profile }} treeview" ><a href="{{ url(route('profile.super')) }}"><i class="fa fa-user"></i> <span>My Profile</span></a></li>