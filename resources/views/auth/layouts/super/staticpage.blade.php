<?php  
  $errorP = false;
  $userS = false;
  $olds = "";
  $whois = new \App\Admin;  
  $whois = $whois->isHe();
  if( Session::has('error') ){
      $errorP = Session::get('error');
      Session::forget('error');
  }
  if( Session::has('userS') ){
      $userS = Session::get('userS');
      Session::forget('userS');
  }
  if(Session::has('old')){
      $olds = Session::get('old'); 
      Session::forget('old');
  }
  if(!isset($edit)){
      $edit['name'] = "";
      $edit['static'] = "";
      $edit['slug'] = "";
      $edit['content'] = "";
      $edit['id'] = "";
      $edit['slides'] = false;
      $media = false;
      $edit['elix'] = false;
  }
  else{
    $edit['content'] = $edit->pagecontent()->first() != null ? $edit->pagecontent()->first()->text : "";
    $edit['slides'] = true;
    $media = $edit->pagecontent()->first() != null ? $edit->pagecontent()->first()->media == null ? false : true : "";
  }
  if($media){
    function media($edit){
      return json_decode($edit->pagecontent()->first()->media);
    }
  }
  else{
    function media($edit){
      return [];
    } 
  }
  function olds($olds,$name){
    if($olds != "")
      return $olds[$name];
  }
?>
@extends('layouts.app_admin')
@section('title',"Add New Page | $admin->name")
@section('menu')
    @include('auth.layouts.super.menu')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ $title }}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="">Pages</li>
        <li class="active">{{ $short }} Page</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="col-md-12 col-md-offset-0">
            <div class="box box-info">
            <!-- form start -->
            <form class="form-horizontal" role="form" method="post" action='{{ url(route($url)) }}' enctype="multipart/form-data" >
            {{ csrf_field() }}
             <input type='hidden' name='id' value='<?= $edit['id'] ?>'>    
              <div class="box-body">
                <div class="clearfix">&nbsp;</div>
                <div class="flash-message">
                    <?php $mm = ''; ?>
                    @foreach(['danger', 'warning', 'success', 'info'] as $msg)
                        <?php
                            if( Session::has('alert-' . $msg) ){
                                $mm = 'alert-'.$msg;
                                $m = Session::get('alert-' . $msg);
                                Session::forget('alert-' . $msg);
                            }
                        ?>
                        @if( $mm == ('alert-'.$msg) )
                            <p class="alert alert-{{ $msg }}">
                                {{ $m }}
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                        @endif
                    @endforeach
                </div>
                
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Page name</label>
                  <div class="col-sm-10">
                    <input type='text' required  name='name' id="name" class='form-control' autocomplete="false" value="<?= olds($olds,'name') != ''? olds($olds,'name') : $edit['name']  ?>" placeholder="Example">
                  </div>
                </div>
                <div class="form-group with-has">
                    <label for="slug" class="col-sm-2 control-label">Permalink</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"> {{ url('/')."/" }}  </span>
                            <input type="text" required name="slug" id="slug" class="form-control" autocomplete="false" value="<?= olds($olds,'slug') != ''? olds($olds,'slug') : $edit['slug']  ?>" placeholder="example" >
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="has-slide" >Has Slideshow</label>
                    <div class="col-sm-10">
                        <input type="checkbox" class="checkbox" id="has-slide" {{ $edit['slug'] == 'welcome' ? 'checked' : '' }}>
                        <input type="hidden" name="hasslide" id="has-slide-y" value='1' {{ $edit['slug'] == 'welcome' ? 'value="1"' : 'value="0"' }}>
                    </div>
                </div>
                <div class="form-group {{ $edit['slides'] ? '' : 'images-holder' }}">
                  <label class="col-sm-2 control-label" >Slides Images</label>
                  @if( ! $edit['slides'] )
                    @for( $i = 1; $i<=5; $i++ )
                      <div class="col-sm-10 {{ $i === 1 ? '' : 'col-sm-offset-2' }} ">
                        <label class="col-sm-2">Image {{ $i }}</label>
                        <div class="col-sm-6 image1">
                          <input type="file" name="image[]" class="form-control" >
                          <input type="text" name="title[]" class="form-control" placeholder="Title {{$i}}">
                          <textarea class="form-control" name="desc[]" placeholder="Description"></textarea>
                        </div>
                        <div class="col-sm-4 slide-image-show">
                        </div>
                      </div>
                      <div class="clear-fix">&nbsp;</div>
                    @endfor
                  @else
                    <?php $i = 1; $cs = count(media($edit)); ?>
                    @foreach(media($edit) as $value )
                      <div class="col-sm-10 {{ $i === 1 ? '' : 'col-sm-offset-2' }} " style="margin-top: 10px;">
                        <label class="col-sm-2">Image {{ $i}}</label>
                        <div class="col-sm-6 image{{$i}}">
                          <input type="file" name="image[]" class="form-control" >
                          <input type="text" name="title[]" class="form-control" placeholder="Title {{$i}}" value="{{ $value->title }}" >
                          <textarea class="form-control" name="desc[]" placeholder="Description">{{ $value->desc }}</textarea>
                        </div>
                        <div class="col-sm-4 slide-image-show">
                          <img src="/img/slides/{{ $value->image }}" alt="{{ $value->title }}" class="img img-responsive">
                        </div>
                      </div>
                      <?php $i++ ?>
                    @endforeach
                    <div class'clearfix'>&nbsp;</div>
                    
                    @for( $o = $i ; $o<=5; $o++ )
                      <div class="col-sm-10 {{ $o === 1 ? '' : 'col-sm-offset-2' }} ">
                        <label class="col-sm-2">Image {{ $o }}</label>
                        <div class="col-sm-6 image{{$o}}">
                          <input type="file" name="image[]" class="form-control" >
                          <input type="text" name="title[]" class="form-control" placeholder="Title {{$o}}">
                          <textarea class="form-control" name="desc[]" placeholder="Description"></textarea>
                        </div>
                        <div class="col-sm-4 slide-image-show">
                        </div>
                      </div>
                      <div class="clear-fix">&nbsp;</div>
                    @endfor
                  @endif
                </div>
                <div class="form-group page-content with-has">
                  <label for='editor' class='col-sm-2 control-label' >Content</label>
                  <div class='col-sm-10'>
                      <textarea id="editor" name="content" required class="form-control"><?= olds($olds,'content') != ''? olds($olds,'content') : $edit['content']  ?></textarea>
                  </div>
                </div>
                  
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <div class="col-sm-7 col-sm-offset-3">
                  <input required type="password" name='password' class="form-control" id="inputPassword3" placeholder="Password">
                  <span class="help-block info" style='font-size: x-small;' >Please fill in your password, to save changes.</span>
                </div>
                <div class="clearfix"></div>
                <button type="submit" class="btn btn-info btn-md col-sm-3 col-md-offset-5">{{ $button }}</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
</div>
@if ( $errorP )
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel2">Error Find</h4>
        </div>
        <div class="modal-body">
            <h3>Incorrect Password !!</h3>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endif
@endsection
@section('style_p')
<link rel="stylesheet" href="{{ asset(url('plugins/select2/select2.min.css')) }}">
<link rel="stylesheet" href="{{ asset(url('plugins/ckeditor/plugins/codesnippet/lib/highlight/styles/default.css')) }}">
@endsection
@section('style')
    
<style type="text/css">
  .invisible{
    visibility: hidden;
  }
</style>
@endsection
@section('script_p')
<script src="{{ asset(url('plugins/select2/select2.full.min.js')) }}"></script>
<script src=" {{ asset(url('plugins/ckeditor/ckeditor.js')) }} "></script>    
@endsection
@section('scripts_top')
<script type="text/javascript">
  $(function(){
    $(".sub-holder").hide();
    $(".images-holder").hide();
    $('.bs-example-modal-sm').modal();
//    $('#editor').ckeditor;  
    //$('.make-password').hide();
  });
</script>
@endsection
@section('script')
<script type="text/javascript">
    var config = {
            // plugins: 'wysiwygarea,toolbar,basicstyles,menubutton,link,sourcearea',
            // extraPlugins: 'widgetbootstrap',
            toolbarGroups: [
                { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
                { name: 'forms', groups: [ 'forms' ] },
                // '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                { name: 'links', groups: [ 'links' ] },
                { name: 'insert', groups: [ 'insert' ] },
                // '/',
                { name: 'styles', groups: [ 'styles' ] },
                { name: 'colors', groups: [ 'colors' ] },
                { name: 'tools', groups: [ 'tools' ] },
                { name: 'others', groups: [ 'others' ] }
            ]
        };

        config.removeButtons = 'Save,NewPage,Preview,Print,Templates';
        config['height'] = 300;		

        CKEDITOR.replace( 'editor', config);  

    $("#name").keyup(function(){
        var str = sansAccent($(this).val());
        str = $.trim(str);
        str = str.replace(/[^a-zA-Z0-9\s]/g,"");
        str = str.toLowerCase();
        str = str.replace(/\s/g,'-');
        $("#slug").val(str);
    });
    var c = 1;
    $("#has-slide").on('click',function(e){
       if( c % 2 == 0 ){
           $(".images-holder").hide();
           $("#has-slide-y").attr("value", function(){
               return "0";
           });
       }
       else if(c % 2 == 1){
           $(".images-holder").fadeIn();
           $("#has-slide-y").attr("value", function(){
               return "1";
           });
       }
       c += 1;
    });
    
</script>
<script type="text/javascript">
//  initSample(); 
  $(function(){
    $(".select2").select2();
  });
</script>
@endsection