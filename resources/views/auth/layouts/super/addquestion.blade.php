<?php  
  $errorP = false;
  $userS = false;
  $olds = "";
  if( Session::has('error') ){
      $errorP = Session::get('error');
      Session::forget('error');
  }
  if(Session::has('old')){
      $olds = Session::get('old'); 
      Session::forget('old');
  }
  if($toedit == false){
      $edit['answers'] = false;
      $edit['credit_id'] = "";
      $edit['question'] = "";
      $edit['id'] = "";
  }
  function olds($olds,$name){
    if($olds != "")
      return $olds[$name];
    else 
        return false;
  }
?>
@extends('layouts.app_admin')
@section('title',"Add New Question | $admin->name")
@section('menu')
    @include('auth.layouts.super.menu')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ $title }}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="">Credits</li>
        <li class="active">{{ $short }} Credit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="col-md-9 col-md-offset-1">
            <div class="box box-info">
            <!-- form start -->
            <form class="form-horizontal" role="form" method="post" action='{{ url(route($url)) }}' enctype="multipart/form-data" >
            {{ csrf_field() }}
             <input type='hidden' name='id' value='<?= $edit['id'] ?>'>    
              <div class="box-body">
                <div class="clearfix">&nbsp;</div>
                <div class="flash-message">
                    <?php $mm = ''; ?>
                    @foreach(['danger', 'warning', 'success', 'info'] as $msg)
                        <?php
                            if( Session::has('alert-' . $msg) ){
                                $mm = 'alert-'.$msg;
                                $m = Session::get('alert-' . $msg);
                                Session::forget('alert-' . $msg);
                            }
                        ?>
                        @if( $mm == ('alert-'.$msg) )
                            <p class="alert alert-{{ $msg }}">
                                {{ $m }}
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                        @endif
                    @endforeach
                </div>
                <div class="form-group">
                  <label for="course" class="col-sm-2 control-label">Credit title</label>
                  <div class="col-sm-10">
                    <select required class="form-control select2" name="credit" >
                      <option selected="selected" value="" disabled >Select from the list </option>
                      @foreach($credits as $credit)
                        <option value="{{ $credit->id }}" 
                            <?php
                                if(olds($olds,'credit') != '' AND olds($olds,'credit') == $credit->id){
                                    echo "selected";
                                }
                                elseif($edit['credit_id'] == $credit->id ){
                                    echo "selected";
                                }
                            ?>
                        >{{ ucfirst($credit->title) }}</option>
                      @endforeach
                    </select>
                    <span class="text-sm text-info">Select from the list above</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="question" class="col-sm-2 control-label">Question</label>
                  <div class="col-sm-10">
                    <input type='text' name='question' id="question" class='form-control' autocomplete="false" value="<?= olds($olds,'question') != false ? olds($olds,'question') : $edit['question']  ?>" placeholder="Question to ask">
                  </div>
                </div>
                <div class="form-group answers">
                    <div class="col-sm-10 col-sm-offset-2">
                        <div class="row">
                            <div class="col-sm-10">Answers</div>
                            <div class="col-sm-2 text-right">Correct</div>
                        </div>
                        <div>&nbsp;</div>
                        @if($toedit == false )
                            @for( $i = 1 ; 5 >= $i ; $i++ )
                                <div class="row">
                                    <div class="col-sm-10">
                                        <input type="text" name="answer_{{$i}}" <?= $i <= 2 ? "required" : "" ?>  value="<?= olds($olds,'answer_'.$i) != false ? trim(olds($olds,'answer_'.$i)) == ""  ? "" : olds($olds,'answer_'.$i) : ''  ?>" class="form-control" placeholder="Answer {{ $i }}" >
                                        @if(2 == $i )
                                            <span class="text-info text-sm">These 2 are required</span>
                                        @endif
                                    </div>
                                    <div class="col-sm-2 ">
                                        <label class="radio pull-right" style="cursor: pointer;">
                                            <input  type="radio" required name="correct" value="<?= 'answer_'.$i  ?>" <?= olds($olds,'correct') != false ? olds($olds,'correct') == 'answer_'.$i? "checked" : "" : "" ?> class="radio"> 
                                            Yes
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix">&nbsp;</div>
                            @endfor
                        @else
                        <?php
                            $answers = count($edit->answer);
                            $i = 1;
                        ?>
                            @foreach( $edit->answer as $answer )
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <input type="text" name="answer_{{$i}}" <?= $i <= 2 ? "required" : "" ?>  value="<?= olds($olds,'answer_'.$i) != false ? trim(olds($olds,'answer_'.$i)) == ""  ? "" : olds($olds,'answer_'.$i) : $toedit ? $answer->answer : ''  ?>" class="form-control" placeholder="Answer {{ $i }}" >
                                            @if(2 == $i )
                                                <span class="text-info text-sm">These 2 are required</span>
                                            @endif
                                        </div>
                                        <div class="col-sm-2 ">
                                            <label class="radio pull-right" style="cursor: pointer;">
                                                <input  type="radio" required name="correct" value="<?= 'answer_'.$i  ?>" <?= olds($olds,'correct') != false ? olds($olds,'correct') == 'answer_'.$i? "checked" : "" : $toedit ? $answer->iscorrect ? 'checked' : '' : '' ?> class="radio"> 
                                                Yes
                                            </label>
                                        </div>
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                            <?php $i++; ?>
                            @endforeach
                        @endif
                        <span class="text-info text-sm">Write the different answers and then check to the correct one</span>
                    </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <div class="col-sm-7 col-sm-offset-3">
                  <input required type="password" name='password' class="form-control" id="inputPassword3" placeholder="Password">
                  <span class="help-block info" style='font-size: x-small;' >Please fill in your password, to save changes.</span>
                </div>
                <div class="clearfix"></div>
                <button type="submit" class="btn btn-info btn-md col-sm-3 col-md-offset-5">{{ $button }}</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
</div>
@if ( $errorP )
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel2">Error Find</h4>
        </div>
        <div class="modal-body">
            <h3>Incorrect Password !!</h3>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endif
@endsection
@section('style_p')
<link rel="stylesheet" href="{{ asset(url('plugins/select2/select2.min.css')) }}">
@endsection
@section('style')
<style type="text/css">
  .invisible{
    visibility: hidden;
  }
</style>
@endsection
@section('script_p')
<script src="{{ asset(url('plugins/select2/select2.full.min.js')) }}"></script>
@endsection
@section('scripts_top')
<script type="text/javascript">
  $(function(){
    $('.bs-example-modal-sm').modal();
    //$('.make-password').hide();
  });
</script>
@endsection
@section('script')
<script type="text/javascript">
  $(function(){
    $(".select2").select2();
  });
</script>
@endsection