<?php  
  $errorP = false;
  if( Session::has('error') ){
      $errorP = Session::get('error');
      Session::forget('error');
  }
?>
@extends('layouts.app_admin')
@section('title',"All Pages  | $admin->name ")
@section('style_p')
  <link rel="stylesheet" href="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset(url('plugins/datatables/dataTables.bootstrap.css')) }}">
@endsection
@section('styles')
<style type="text/css">
  .on{
    text-align: left;
    background: #16A085;
    color: #fff;
    cursor: pointer;
  }
  .on .fa ,.off .fa{
    padding-top: .25em; 
  }
  .on:hover{
    color: #fff;
  }
  .off:click{
    box-shadow: none;
  }
  .off{
    text-align: right;
    cursor: none;
    color: #fff;
    background-color: #34495E; 
  }
</style>
@endsection
@section('menu')
    @include('auth.layouts.super.menu')
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Pages
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">pages</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div class="box box-solid">
            <div class="box-body">
              <table id="example2" class="table table-hover">
                    <thead>
                    <tr>
                      <th>Page Name</th>
                      <th>SubPages</th>
                      <th>Status</th>            
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
                       <?php $i= 1; ?>  
                       @foreach($pages as $page)
                       <?php modalDelete($i,$page->name,$page->id); ?>
                        <tr>
                          <td>{{ $page->name }}</td>
                          <td>
                            <label class="label label-primary">Has Sub Page : {{ count($page->subpage)  }} </label>
                            <div>&nbsp;</div>
                            @if(count($page->subpage))
                               <table class="table" >
                               @foreach($page->subpage as $subpage )
                                <tr class="{{ $subpage->valid ? 'on' : 'off' }}">
                                  <td>
                                    <a href="{{ url( route('edit.subpage.super',$subpage->id) ) }}" class="a" >{{ ucfirst($subpage->name) }}</a>
                                  </td>
                                  <td><button class='btn btn-xs  btn-warning pull-right del-sub' value='".$subpage->id."' >X</button> </td>
                                </tr>
                               <?php modalDeleteSub($subpage->id,$subpage->name,$subpage->id) ?>
                              @endforeach  
                               </table>
                            @endif
                          </td>
                          <td>
                            <label class="label label-primary">Is Static : {{ $page->static ? 'Yes' : 'No' }} </label>
                          </td>
                          <td>
                            <button class="btn btn-sm btn-flat bg-maroon pull-right btn-delete-page" value="{{ $i }}">Delete</button>
                            <a class="btn btn-sm btn-flat bg-olive pull-right" href="{{ url(route('with.slug.web',$page->slug)) }}" target="_blank"  >View</a>
                            <a class="btn btn-sm btn-flat bg-navy pull-right" href="<?php
                              $route = 'edit.page.super';
                              $url = route($route,$page->id);
                              if($page->static){
                                $route = 'static.page.super';
                                $url = url(route($route).'/'.$page->id);
                              }
                              echo $url;
                            ?>">Edit</a>
                          </td>
                        </tr>
                        <?php $i++;  ?>
                       @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                          <th>Page Name</th>
                          <th>SubPages</th>
                          <th>Status</th>            
                          <th></th>
                      </tr>
                    </tfoot>
              </table>  
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection
@if ( $errorP )
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel2">Error Find</h4>
        </div>
        <div class="modal-body">
            <h3>Incorrect Password !!</h3>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endif
@section('script_p')
  <!-- DataTables -->
  <script src="{{ asset(url('plugins/datatables/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(url('plugins/datatables/dataTables.bootstrap.min.js')) }}"></script>
  <!-- FastClick -->
  <script src="{{ asset(url('plugins/fastclick/fastclick.js')) }}"></script>
@endsection
@section('script')
  <script type="text/javascript">
    $(function(){
      $('.bs-example-modal-sm').modal();
         
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
      });
//      $('.btn-delete').on('click',function(e){
//          var b = e.target.value;
//      });    
      $('* .btn-delete-page').on('click',function(e){
        var b = e.target.value;
        $("*#modalTitle").text('Delete ');
        $('.bs-modal-'+b).modal();
      });

    });
  </script>
@endsection
@section('scripts_top')
@endsection
<?php
function modalDelete($i,$title,$id){
?>
<div class="modal modal-primary fade modal-top-20 bs-modal-{{ $i }}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">
          <span id="modalTitle"></span>&nbsp;
          <span style="text-transform: uppercase;">{{ $title }}</span> 
        </h4>
      </div>
      <div class="modal-body">
          <form action="{{ url(route('destroy.page.super')) }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="page" value="{{ $id }}" >
            <div class="form-group">
              <input required type="password" autofocus name='password' class="form-control border-none radius-2 passwordS" id="inputPassword3" placeholder="Password">
              <span class="help-block info" style='font-size: x-small;color: #fff;' >Please fill in your password and press ENTER, to delete this page.</span>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
<?php  
}
function modalDeleteSub($i,$title,$id){
?>
<div class="modal modal-primary fade modal-top-20 bs-modal-s-{{ $i }}" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">
          <span id="modalTitle"></span>&nbsp;
          <span style="text-transform: uppercase;">{{ $title }}</span> 
        </h4>
      </div>
      <div class="modal-body">
          <form action="{{ url(route('destroy.page.super')) }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="page" value="{{ $id }}" >
            <div class="form-group">
              <input required type="password" autofocus name='password' class="form-control border-none radius-2 passwordS" id="inputPassword3" placeholder="Password">
              <span class="help-block info" style='font-size: x-small;color: #fff;' >Please fill in your password and press ENTER, to delete this page.</span>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
<?php  
}
?>