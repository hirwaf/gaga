@extends('layouts.app_admin')
@section('title',"Profile | $admin->name")
@section('menu')
    @include('auth.layouts.super.menu')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-9 col-md-offset-1">
            <div class="box box-info">
            <!-- form start -->
            <form class="form-horizontal" role="form" method="post" action='{{ url("/admin/profile") }}' >
            {{ csrf_field() }}
              <div class="box-body">
                <div class="clearfix">&nbsp;</div>
                <div class="flash-message">
                    <?php $mm = ''; ?>
                    @foreach(['danger', 'warning', 'success', 'info'] as $msg)
                        <?php
                            if( Session::has('alert-' . $msg) ){
                                $mm = 'alert-'.$msg;
                                $m = Session::get('alert-' . $msg);
                                Session::forget('alert-' . $msg);
                            }
                        ?>
                        @if( $mm == ('alert-'.$msg) )
                            <p class="alert alert-{{ $msg }}">
                                {{ $m }}
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                        @endif
                    @endforeach
                </div>
                <div class="form-group">
                  <label for="name" class="col-sm-2 control-label">Names</label>
                  <div class="col-sm-10">
                    <input required type="text" id='name' name="name" class="form-control" id="email" value="{{ $admin->name }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="email" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-10">
                    <input required type="email" class="form-control" name="email" id="email" value="{{ $admin->email }}">
                  </div>
                </div>
                  <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">Phone</label>  
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input required type="text" class="form-control" name="phone" id="phone" data-inputmask='"mask": "+250(###)###-###" ' data-mask value = "{{ $admin->phone}}">
                            <div class="input-group-addon">
                                <i class="fa fa-phone"></i>
                            </div>
                        </div>   
                    </div>
                  </div>
                <div class='clearfix c1'>&nbsp;</div>
                <div id='changepassword'>
                    <div class="form-group">
                      <label for="inputPassword4" class="col-sm-2 control-label">New Password</label>
                      <div class="col-sm-10">
                        <input type="password" name='new_password' class="form-control" id="inputPassword4" placeholder="New Password">
                      </div>  
                    </div>
                    <div class="form-group">
                      <label for="inputPassword5" class="col-sm-2 control-label">Confirm Password</label>
                      <div class="col-sm-10">
                        <input type="password" name='new_password_confirmation' class="form-control" id="inputPassword5" placeholder="Confirm Password">
                      </div>  
                    </div>
                </div>  
                <div class='clearfix' style="border-top: solid 2px #3498DB;">&nbsp;</div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-10">
                    <input required type="password" name='password' class="form-control" id="inputPassword3" placeholder="Password">
                    <span class="help-block" style="color:#C0392B;">Please fill in your <span id='ct'>current</span> password, to save changes.</span>
                  </div>  
                </div>
                <?php  
                  $errorP = false;
                  if( Session::has('error') ){
                      $errorP = Session::get('error');
                      Session::forget('error');
                  }
                ?>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <div class="checkbox pull-left">
                  <label>
                    <input type="checkbox" id='cp'> I want to change PASSWORD
                  </label>
                </div>  
                <button type="submit" class="btn btn-info pull-right">Update</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        </div> 
      </div>
    </section>
    <!-- /.content -->
  </div>
  @if ( $errorP )
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel2">Error Find</h4>
          </div>
          <div class="modal-body">
              <h3>Incorrect Password !!</h3>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  @endif
@endsection
@section('scripts_top')

<script type="text/javascript">
    $(function(){
        $('.bs-example-modal-sm').modal();

        $("#inputPassword4,#inputPassword5").attr('disabled',true);
        $('#changepassword').hide();
        $('#ct').hide();
        $('.c1').css({
            "border-top": "solid 0px #BDC3C7"
        });
         var val = 1;
        $("#cp").click(function(){
            while(val >= 1 ){
                if(val%2 != 0){
                    $("#inputPassword4,#inputPassword5").attr('disabled',false);
                    $('#changepassword').fadeIn();
                    $('#ct').fadeIn();
                    $('.c1').css({
                        "border-top": "solid 2px #3498DB"
                    });
                }
                else if(val%2 == 0){
                    $("#inputPassword4,#inputPassword5").attr('disabled',true);
                    $('#changepassword').hide();
                    $('#ct').hide();
                    $('.c1').css({
                        "border-top": "solid 0px #BDC3C7"
                    });
                }
                val += 1;
                break;
            };
            if($(this).val()){
                
            }else{
                
            };
        });
    });
</script>

@endsection