@extends('layouts.app_admin')
@section('title',"Subscribers | $admin->name ")
@section('styles')
<link rel="stylesheet" href="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
@endsection

@section('menu')
    @include('auth.layouts.super.menu')
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Subscribers <small><label class="label label-info">{{ $repo->subCount() }}</label></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Subscribers</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        @if( $repo->subCount() > 0 )
        @foreach( $subscribes as $subscribe )
        <div class="col-sm-3">
          <h3 style="
            background-color: skyblue;
            padding: .4em;
            box-shadow: 0px 1px 6px rgba(0,0,0,.2);
          ">{{ $subscribe->email }}</h3>
        </div>
        @endforeach
        <div class="col-sm-12 col-sm-offset-5">
          {{ $subscribes->links() }}
        </div>
        @else
        <div class="col-sm-12 text-center">
          <h3>We havan't any subscriber</h3>
        </div>
        @endif
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection
@section('script_p')

@endsection

@section('script')

@endsection