<?php  
  $errorP = false;
  if( Session::has('error') ){
      $errorP = Session::get('error');
      Session::forget('error');
  }
?>
@extends('layouts.app_admin')
@section('title',"All Credits  | $admin->name ")
@section('style_p')
  <link rel="stylesheet" href="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset(url('plugins/datatables/dataTables.bootstrap.css')) }}">
@endsection
@section('styles')
<style type="text/css">
    #btn-delete{
        margin-top: 4.5em;
    }
    .no{
        background-color: #ECF0F1;
    }
    .yes{
        background-color: #16A085;
        font-weight: 500;
        color: #fff;
    }
</style>
@endsection
@section('menu')
    @include('auth.layouts.super.menu')
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Credits
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Credits</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-md-offset-0">
          <div class="box">
            <div class="box-body">
              <table id="example2" class="table table-hover">
                <thead>
                <tr>
                  <th width='220px'>Question</th>
                  <th>Credit</th>
                  <th>Answers</th>  
                  <th>Created</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                  @foreach($questions as $question)
                    <tr>
                      <td style="cursor: pointer;text-transform:capitalize;" width="200" class="title-{{$i}}" >
                          <a href="{{ url(route('edit.question.super',$question->id)) }}" >  {{ $i." . ".ucfirst($question->question) }} </a><br>
                      </td>
                      <td style="cursor: pointer;text-transform:capitalize;" width="200" >
                          {{ ucfirst($question->credit->title) }}
                      </td>
                      <td>
                          <table class="table">
                            <tr>
                                <th>Answer</th>
                                <th>Is Correct</th>
                            </tr>
                            @foreach($question->answer as $answer )
                              <tr class=" {{ $answer->iscorrect ? 'yes' : 'no' }} " >
                                <td>{{ ucfirst($answer->answer) }}</td>
                                <td><?= $answer->iscorrect ? "Yes" : "No <button class='btn btn-xs  btn-warning pull-right del-ans' value='".$answer->id."' >X</button> " ?> </td>
                              </tr>
                              <?php modalDelete('a-'.$answer->id,$answer->answer,$answer->id,'answer','destroy.answer.super') ?>
                            @endforeach
                          </table>
                      </td>
                      <td><label class="label label-primary label-sm">{{ $question->created_at }}</label></td>
                      <td>
                          <center>
                              <button class="btn btn-sm btn-flat btn-danger btn-delete destroy" id="btn-delete" value="{{ $i }}" >Delete</button>
                          </center>
                      </td>
                    </tr>
                    <?php 
                        #modalTitle($i,$course->course,$course->id);
                        modalDelete($i,$question->question,$question->id); 
                        $i++;
                    ?>
                  @endforeach
                </tbody>
                <tfoot>
                  <tr>
                      <th width='220px'>Question</th>
                      <th>Credit</th>
                      <th>Answers</th>  
                      <th>Created</th>
                      <th></th>
                  </tr>
                </tfoot>
              </table>              
            </div>
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
@endsection
@if ( $errorP )
  <div class="modal fade bs-example-modal-sm" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel2">Error Find</h4>
        </div>
        <div class="modal-body">
            <h3>Incorrect Password !!</h3>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endif
@section('script_p')
  <!-- DataTables -->
  <script src="{{ asset(url('plugins/datatables/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(url('plugins/datatables/dataTables.bootstrap.min.js')) }}"></script>
  <!-- FastClick -->
  <script src="{{ asset(url('plugins/fastclick/fastclick.js')) }}"></script>
@endsection
@section('script')
  <script type="text/javascript">
    $(function(){
      $('.bs-example-modal-sm').modal();
         
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
      });
//      $('.btn-delete').on('click',function(e){
//          var b = e.target.value;
//      });    
      $('*.destroy').on('click',function(e){
        var b = e.target.value;
        $("*#modalTitle").text('Delete ');
        $('.bs-modal-'+b).modal();
      });
      $('*.del-ans').on('click', function(e){
          var l = e.target.value;
          $('.bs-modal-a-'+l).modal();
      });  
    
    });
  </script>
@endsection
@section('scripts_top')
@endsection
<?php
function modalDelete($i,$title,$id,$text='question',$url='destroy.question.super'){
?>
<div class="modal modal-primary fade modal-top-20 bs-modal-{{ $i }}" tabindex="1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">
          <span id="modalTitle">Delete</span>&nbsp;
          <span style="text-transform: uppercase;">{{ $title }}</span> 
        </h4>
      </div>
      <div class="modal-body">
          <form action="{{ url(route($url)) }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="question" value="{{ $id }}" >
            <div class="form-group">
              <input required type="password" autofocus='true' name='password' class="form-control border-none radius-2 passwordS" id="inputPassword3" placeholder="Password">
              <span class="help-block info" style='font-size: x-small;color: #fff;' >Please fill in your password and press ENTER, to delete this {{ $text }}.</span>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>
<?php  
}
?>