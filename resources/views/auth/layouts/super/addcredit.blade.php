<?php  
  $errorP = false;
  $userS = false;
  $olds = "";
  if( Session::has('error') ){
      $errorP = Session::get('error');
      Session::forget('error');
  }
  if( Session::has('userS') ){
      $userS = Session::get('userS');
      Session::forget('userS');
  }
  if(Session::has('old')){
      $olds = Session::get('old'); 
      Session::forget('old');
  }
  if(!isset($edit)){
      $edit['course_id'] = "";
      $edit['title'] = "";
      $edit['summary'] = "";
      $edit['id'] = "";
  }
  function olds($olds,$name){
    if($olds != "")
      return $olds[$name];
  }
?>
@extends('layouts.app_admin')
@section('title',"Add New Course | $admin->name")
@section('menu')
    @include('auth.layouts.super.menu')
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ $title }}
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url(route('dashboard.super')) }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="">Credits</li>
        <li class="active">{{ $short }} Credit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      <div class="col-md-9 col-md-offset-1">
            <div class="box box-info">
            <!-- form start -->
            <form class="form-horizontal" role="form" method="post" action='{{ url(route($url)) }}' enctype="multipart/form-data" >
            {{ csrf_field() }}
             <input type='hidden' name='id' value='<?= $edit['id'] ?>'>    
              <div class="box-body">
                <div class="clearfix">&nbsp;</div>
                <div class="flash-message">
                    <?php $mm = ''; ?>
                    @foreach(['danger', 'warning', 'success', 'info'] as $msg)
                        <?php
                            if( Session::has('alert-' . $msg) ){
                                $mm = 'alert-'.$msg;
                                $m = Session::get('alert-' . $msg);
                                Session::forget('alert-' . $msg);
                            }
                        ?>
                        @if( $mm == ('alert-'.$msg) )
                            <p class="alert alert-{{ $msg }}">
                                {{ $m }}
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            </p>
                        @endif
                    @endforeach
                </div>
                <div class="form-group">
                  <label for="course" class="col-sm-2 control-label">Course title</label>
                  <div class="col-sm-10">
                    <select required class="form-control select2" name="course" >
                      <option selected="selected" value="" disabled >Select from the list </option>
                      @foreach($courses as $course)
                        <option value="{{ $course->id }}" 
                            <?php
                                if(olds($olds,'course') != '' AND olds($olds,'course') == $course->id){
                                    echo "selected";   
                                }
                                elseif($edit['course_id'] == $course->id ){
                                    echo "selected";
                                }
                            ?>
                        >{{ ucfirst($course->course) }}</option>
                      @endforeach
                    </select>
                    <span class="text-sm text-info">Select from the list above</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="credit" class="col-sm-2 control-label">Credit title</label>
                  <div class="col-sm-10">
                    <input type='text' name='creditTitle' id="credit" class='form-control' autocomplete="false" value="<?= olds($olds,'creditTitle') != ''? olds($olds,'creditTitle') : $edit['title']  ?>" placeholder="Credit Title">
                  </div>
                </div>
                <div class="form-group">
                  <label for="content" class="col-sm-2 control-label">Credit content</label>
                  <div class="col-sm-10">
                    <input type='file' name='content' id="content" class='form-control' title="Upload PDF File"  placeholder="Credit content">
                    <span class="text-sm text-info">Upload a PDF file</span>  
                  </div>
                </div>                  
                <div class="form-group">
                  <label for='summary' class='col-sm-2 control-label' >Summary</label>
                  <div class='col-sm-10'>
                    <textarea name="summary" id="summary" rows="5" class="form-control" placeholder="Summary on the course"><?= olds($olds,'summary') != ''? olds($olds,'summary') : $edit['summary']  ?></textarea>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <div class="col-sm-7 col-sm-offset-3">
                  <input required type="password" name='password' class="form-control" id="inputPassword3" placeholder="Password">
                  <span class="help-block info" style='font-size: x-small;' >Please fill in your password, to save changes.</span>
                </div>
                <div class="clearfix"></div>
                <button type="submit" class="btn btn-info btn-md col-sm-3 col-md-offset-5">{{ $button }}</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
      </div>
    </section>
    <!-- /.content -->
</div>
@if ( $errorP )
  <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel2">Error Find</h4>
        </div>
        <div class="modal-body">
            <h3>Incorrect Password !!</h3>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
@endif
@endsection
@section('style_p')
<link rel="stylesheet" href="{{ asset(url('plugins/select2/select2.min.css')) }}">
@endsection
@section('style')
<style type="text/css">
  .invisible{
    visibility: hidden;
  }
</style>
@endsection
@section('script_p')
<script src="{{ asset(url('plugins/select2/select2.full.min.js')) }}"></script>
@endsection
@section('scripts_top')
<script type="text/javascript">
  $(function(){
    $('.bs-example-modal-sm').modal();
    //$('.make-password').hide();
  });
</script>
@endsection
@section('script')
<script type="text/javascript">
  $(function(){
    $(".select2").select2();
  });
</script>
@endsection