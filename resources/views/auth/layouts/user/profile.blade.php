@extends('layouts.app')

@section('title',ucfirst($user->get()->firstname)." ".ucfirst($user->get()->lastname)." | ".$user->getitle())

@section('menu')
    @include('layouts.menu')
@endsection

@section('content')
<div class="row">
	<div class="col-md-3">
		<img src="{{ asset( $user->getProfile() ) }}" class="img-responsive img-thumbnail">
	</div>
  <div class="col-md-9">
      <h3 class='text-left' style="text-transform: uppercase;">{{ $user->get()->firstname }}&nbsp;{{ $user->get()->lastname }}</h3>
      <div class="row">
        <div class="col-md-12">
            <label class="label label-primary" style="font-size: medium;">
                Location: <span style="text-transform: capitalize;">{{ $user->getAddress() }}</span>
            </label>
            <div class="clearfix">&nbsp;</div>
        </div>
        <div class="col-md-12">
            <label class="label label-primary" style="font-size: medium;">
                Telephone: <span style="text-transform: capitalize;">{{ $user->get()->telephone }}</span>
            </label>
            <div class="clearfix">&nbsp;</div>
        </div>
        <div class="col-md-12">
            <label class="label label-primary" style="font-size: medium;">
                Email: <span style="text-transform: capitalize;">{{ $user->get()->email }}</span>
            </label>
            <div class="clearfix">&nbsp;</div>
        </div>
        <div class="col-md-12">

          @if( $user->get()->approved )
          <label class="label label-success">Approved: Yes</label>
          @else
          <label class="label label-warning">Approved: No</label>
          @endif
        </div>
      </div>
  </div>
</div>
<div class="clear-fix">&nbsp;</div>
@endsection

@section('styles')

@endsection

@section('scripts')
<script type="text/javascript">
	$(function(){
	});
</script>
@endsection
