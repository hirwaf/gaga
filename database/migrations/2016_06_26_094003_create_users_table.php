<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users',function(Blueprint $table){
            $table->increments('id');
            $table->integer('information_id')->unsigned();
            $table->foreign('information_id')->references('id')->on('information')->onDelete('cascade');
            $table->string('email')->unique();
            $table->string('telephone')->unique();
            $table->string('password');
            $table->boolean('active')->default(false);
            $table->string('remember_token',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
