<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagecontents', function(Blueprint $table){
            $table->increments('id');
            $table->integer('page_id')->unsigned();
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
            $table->integer('sub_page_id')->unsigned()->nullable();
            $table->foreign('sub_page_id')->references('id')->on('sub_pages')->onDelete('cascade');
            $table->text('media')->nullable();
            $table->string('meta')->nullable();
            $table->string('behevior')->nullable();
            $table->boolean('issub')->default(false);
            $table->text('text');
            $table->boolean('isvalid')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pagecontents');
    }
}
