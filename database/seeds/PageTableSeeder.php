<?php

use Illuminate\Database\Seeder;
use App\Page;
use App\Menu;

class PageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $pages = [
        	[
        		'name'			=>	'Welcome',
        		'slug'			=>	'welcome',
        		'static' 		=> 	true,
        		'elix'			=>	true,
        		'created_at'	=>	'2016-07-14 13:56:38',
        		'updated_at'	=>	'2016-07-14 13:56:38'
        	],
        	[
        		'name'			=>	'Courses',
        		'slug'			=>	'courses',
        		'elix'			=>	true,
        		'created_at'	=>	'2016-07-14 13:56:38',
        		'updated_at'	=>	'2016-07-14 13:56:38'
        	],
        	[
        		'name'			=>	'Registration',
        		'slug'			=>	'registration',
        		'elix'			=>	true,
        		'created_at'	=>	'2016-07-14 13:56:38',
        		'updated_at'	=>	'2016-07-14 13:56:38'
        	],
        	[
        		'name'			=>	'Payments',
        		'slug'			=>	'payments',
        		'elix'			=>	true,
        		'created_at'	=>	'2016-07-14 13:56:38',
        		'updated_at'	=>	'2016-07-14 13:56:38'
        	],
        	[
        		'name'			=>	'Certificates',
        		'slug'			=>	'certificates',
        		'elix'			=>	true,
        		'created_at'	=>	'2016-07-14 13:56:38',
        		'updated_at'	=>	'2016-07-14 13:56:38'
        	],
        	[
        		'name'			=>	'Gallery',
        		'slug'			=>	'gallery',
        		'elix'			=>	true,
        		'created_at'	=>	'2016-07-14 13:56:38',
        		'updated_at'	=>	'2016-07-14 13:56:38'	
        	],
        	[
        		'name'			=>	'Quiz',
        		'slug'			=>	'quiz',
        		'elix'			=>	true,
        		'created_at'	=>	'2016-07-14 13:56:38',
        		'updated_at'	=>	'2016-07-14 13:56:38'
        	],
        	[
        		'name'			=>	'Login',
        		'slug'			=>	'login',
        		'elix'			=>	true,
        		'created_at'	=>	'2016-07-14 13:56:38',
        		'updated_at'	=>	'2016-07-14 13:56:38'
        	],
        	[
        		'name'			=>	'Statistics',
        		'slug'			=>	'statistics',
        		'elix'			=>	true,
        		'created_at'	=>	'2016-07-14 13:56:38',
        		'updated_at'	=>	'2016-07-14 13:56:38'
        	],
        	[
        		'name'			=>	'About us',
        		'slug'			=>	'about-us',
        		'elix'			=>	true,
        		'created_at'	=>	'2016-07-14 13:56:38',
        		'updated_at'	=>	'2016-07-14 13:56:38'
        	],
        	[
        		'name'			=>	'Contact us',
        		'slug'			=>	'contact-us',
        		'elix'			=>	true,
        		'created_at'	=>	'2016-07-14 13:56:38',
        		'updated_at'	=>	'2016-07-14 13:56:38'
        	]
        ];

        foreach ( $pages as $key => $page ) {
        	$page = Page::create($page);
        	Menu::create([
        		'page_id'	=>	$page->id,
        		'order'		=>	$key++,
        		'active'	=> 	($key == 11 )? false : true,
        		'auth'		=>	( $key == 1 OR $key == 2 )? true : false,
        		'only'		=>	( $key == 5 OR $key == 7 OR $key == 9 )? true : false
        	]);	
        }

    }
}
