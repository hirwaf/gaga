<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('options')->insert([
            'name' 	=> 'sitetitle',
            'value' => null,
            'valid' =>  true
        ]);
        DB::table('options')->insert([
            'name' 	=> 'tagline',
            'value' => null,
            'valid' =>  true
        ]);
        DB::table('options')->insert([
            'name' 	=> 'email',
            'value' => null,
            'valid' =>  true
        ]);
        DB::table('options')->insert([
            'name' 	=> 'phone1',
            'value' => null,
            'valid' =>  true
        ]);
        DB::table('options')->insert([
            'name' 	=> 'phone2',
            'value' => null,
            'valid' =>  true
        ]);
        DB::table('options')->insert([
            'name' 	=> 'phone3',
            'value' => null,
            'valid' =>  true
        ]);
        DB::table('options')->insert([
            'name' 	=> 'phone4',
            'value' => null,
            'valid' =>  true
        ]);
        DB::table('options')->insert([
            'name' 	=> 'phone5',
            'value' => null,
            'valid' =>  true
        ]);
        DB::table('options')->insert([
        	'name'	=>	'map',
        	'value'	=>	null,
        	'valid'	=>	true
        ]);
        DB::table('options')->insert([
            'name' 	=> 'facebook',
            'value' => null,
            'valid' =>  true
        ]);
        DB::table('options')->insert([
            'name' 	=> 'twitter',
            'value' => null,
            'valid' =>  true
        ]);
        DB::table('options')->insert([
            'name' 	=> 'googleplus',
            'value' => null,
            'valid' =>  true
        ]);
        DB::table('options')->insert([
            'name' 	=> 'linkedin',
            'value' => null,
            'valid' =>  true
        ]);

        DB::table('options')->insert([
            'name'  => 'icon',
            'value' => null,
            'valid' =>  true
        ]);        
    }
}
