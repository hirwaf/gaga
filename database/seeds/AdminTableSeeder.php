<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            'name' => 'hirwa felix',
            'email' => 'hirwa.felix@gmail.com',
            'phone' =>  '+250789812404',
            'password' => bcrypt('secret'),
        ]);
        DB::table('admins')->insert([
            'name' => 'ngamba damascene',
            'email' => 'ngamba@yahoo.com',
            'phone' =>  '+250700000000',
            'password' => bcrypt('ngamba'),
        ]);
    }
}
