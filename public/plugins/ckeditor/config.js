/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.skin = 'moono-dark',
	config.language = 'en',
	config.extraPlugins = 'bootstrapVisibility';
	config.filebrowserBrowseUrl = '/filemanager/browse.php?opener=ckeditor&type=files';
	config.filebrowserImageBrowseUrl = '/filemanager/browse.php?opener=ckeditor&type=images';
	config.filebrowserFlashBrowseUrl = '/filemanager/browse.php?opener=ckeditor&type=flash';
	config.filebrowserUploadUrl = '/filemanager/upload.php?opener=ckeditor&type=files';
	config.filebrowserImageUploadUrl = '/filemanager/upload.php?opener=ckeditor&type=images';
	config.filebrowserFlashUploadUrl = '/filemanager/upload.php?opener=ckeditor$type=flash';
};
